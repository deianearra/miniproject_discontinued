﻿
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class ProfileRepo
    {

        public static BiodataViewModel ById(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(o => o.Id == id)
                    .Select(o => new BiodataViewModel
                    {
                        Image_Path = o.Image_Path
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Edit(BiodataViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    biodata loc = db.biodatas
                         .Where(o => o.Id == entity.Id)
                         .FirstOrDefault();
                    if (loc != null)
                    {
                        loc.Image_Path = entity.Image_Path;

                        loc.Modified_By = (int?)entity.Modified_By;
                        loc.Modified_On = DateTime.Now;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }
    }
}
