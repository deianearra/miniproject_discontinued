﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class KatFasKesRepo
    {
        public static List<KategoriFasilitasKesehatanViewModel> All()
        {
            List<KategoriFasilitasKesehatanViewModel> result = new List<KategoriFasilitasKesehatanViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from c in db.m_medical_facility_category
                          select new KategoriFasilitasKesehatanViewModel
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return result;
        }
        public static Tuple<List<KategoriFasilitasKesehatanViewModel>, int> All(int page, int rows)
        {
            List<KategoriFasilitasKesehatanViewModel> result = new List<KategoriFasilitasKesehatanViewModel>();
            int rowNum = 0;
            using(var db = new XsisPosDbContext())
            {
                var query = db.m_medical_facility_category;
                rowNum = query.Count();
                result = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                    .Select(c => new KategoriFasilitasKesehatanViewModel
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Created_By = c.Created_By,
                        Created_On = c.Created_On,
                        Modified_By = c.Modified_By,
                        Modified_On = (DateTime)c.Modified_On,
                        Deleted_by = c.Deleted_by,
                        Deleted_On = (DateTime)c.Deleted_On,
                        Is_Delete = c.Is_Delete
                    }).ToList();
            }
            return new Tuple<List<KategoriFasilitasKesehatanViewModel>, int>(result, rowNum);
        }
        public static List<KategoriFasilitasKesehatanViewModel> ByFilter(string search)
        {
            List<KategoriFasilitasKesehatanViewModel> result = new List<KategoriFasilitasKesehatanViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_medical_facility_category
                    .Where(o => o.Name.Contains(search));
                result = query.Take(5)
                    .Select(o => new KategoriFasilitasKesehatanViewModel
                    {
                        Id = o.Id,
                        Name = o.Name,
                        Created_By = o.Created_By,
                        Created_On = o.Created_On,
                        Modified_By = o.Modified_By,
                        Modified_On = (DateTime)o.Modified_On,
                        Deleted_by = o.Deleted_by,
                        Deleted_On = (DateTime)o.Deleted_On,
                        Is_Delete = o.Is_Delete
                    }).ToList();
            }
            return result;
        }
        public static KategoriFasilitasKesehatanViewModel ById(long id)
        {
            KategoriFasilitasKesehatanViewModel result = new KategoriFasilitasKesehatanViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_medical_facility_category
                    .Where(o => o.Id == id)
                    .Select(o => new KategoriFasilitasKesehatanViewModel
                    {
                        Id = o.Id,
                        Name = o.Name,
                        Created_By = o.Created_By,
                        Created_On = (DateTime)o.Created_On,
                        Modified_By = o.Modified_By,
                        Modified_On = (DateTime)o.Modified_On,
                        Is_Delete = o.Is_Delete
                    }).FirstOrDefault();
            }
            return result;
        }
        public static bool Create(KategoriFasilitasKesehatanViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Kategori_Fasilitas_Kesehatan prod = new Kategori_Fasilitas_Kesehatan();

                    prod.Name = entity.Name;
                    prod.Created_By = entity.Created_By;
                    prod.Created_On = DateTime.Now;
                    prod.Is_Delete = false;

                    db.m_medical_facility_category.Add(prod);
                    db.SaveChanges();
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool Edit(Kategori_Fasilitas_Kesehatan entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Kategori_Fasilitas_Kesehatan prod = db.m_medical_facility_category
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault(); //kembali ke element pertama jika element tidak diketemukan
                    if (prod != null)
                    {
                        prod.Name = entity.Name;

                        prod.Modified_By = entity.Modified_By;
                        prod.Modified_On = DateTime.Now;
                        prod.Is_Delete = false;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool Delete(long id)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Kategori_Fasilitas_Kesehatan prod = db.m_medical_facility_category
                        .Where(o => o.Id == id)
                        .FirstOrDefault();
                    if (prod != null)
                    {
                        db.m_medical_facility_category.Remove(prod);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
    }
}
