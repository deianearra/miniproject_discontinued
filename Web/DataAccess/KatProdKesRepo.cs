﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class KatProdKesRepo
    {
        public static List<KategoriProdukKesehatanViewModel> All()
        {
            List<KategoriProdukKesehatanViewModel> result = new List<KategoriProdukKesehatanViewModel>();
            using (var db = new XsisPosDbContext())
            {

                result = (from c in db.m_medical_item_category
                          select new KategoriProdukKesehatanViewModel
                          {
                              Id = c.Id,
                              Name = c.Name

                          }).ToList();
            }
            return result;
        }
        public static Tuple<List<KategoriProdukKesehatanViewModel>, int> All(int page, int rows)
        {
            // Page
            // Berapa baris @ page 
            // total 105 rows, Req page 2 masing" 10 Rows/page
            List<KategoriProdukKesehatanViewModel> result = new List<KategoriProdukKesehatanViewModel>();

            int rowNum = 0;
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_medical_item_category;
                rowNum = query.Count();
                result = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                    .Select(c => new KategoriProdukKesehatanViewModel
                    {
                        Id = c.Id,
                        Name = c.Name,
                        Created_By = c.Created_By,
                        Created_On = c.Created_On,
                        Modified_By = c.Modified_By,
                        Modified_On = (DateTime)c.Modified_On,
                        Deleted_by = c.Deleted_by,
                        Deleted_On = (DateTime)c.Deleted_On,
                        Is_Delete = c.Is_Delete

                    }).ToList();
            }
            // tuple adalah pengembalian nilai lebih dari satu tipe data
            return new Tuple<List<KategoriProdukKesehatanViewModel>, int>(result, rowNum);
        }
        public static List<KategoriProdukKesehatanViewModel> ByFilter(string search)
        {
            List<KategoriProdukKesehatanViewModel> result = new List<KategoriProdukKesehatanViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_medical_item_category
                    .Where(o => o.Name.Contains(search));

                result = query.Take(5)
                          .Select(o => new KategoriProdukKesehatanViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              Created_By = o.Created_By,
                              Created_On = o.Created_On,
                              Modified_By = o.Modified_By,
                              Modified_On = (DateTime)o.Modified_On,
                              Deleted_by = o.Deleted_by,
                              Deleted_On = (DateTime)o.Deleted_On,
                              Is_Delete = o.Is_Delete

                          }).ToList();
            }
            return result;
        }

        public static KategoriProdukKesehatanViewModel ById(long id)
        {
            KategoriProdukKesehatanViewModel result = new KategoriProdukKesehatanViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_medical_item_category
                    .Where(o => o.Id == id)
                    .Select(o => new KategoriProdukKesehatanViewModel
                    {
                        Id = o.Id,
                        Name = o.Name,
                        Created_By = o.Created_By,
                        Created_On = (DateTime)o.Created_On,
                        Modified_By = o.Modified_By,
                        Modified_On = (DateTime)o.Modified_On,
                        Is_Delete = o.Is_Delete
                    }).FirstOrDefault();
            }
            return result;

        }

        public static bool Create(KategoriProdukKesehatanViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Katagori_Produk_Kesehatan prod = new Katagori_Produk_Kesehatan();

                    prod.Name = entity.Name;
                    prod.Created_By = entity.Created_By;
                    prod.Created_On = DateTime.Now;
                    prod.Is_Delete = false;

                    db.m_medical_item_category.Add(prod);
                    db.SaveChanges();
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Edit(KategoriProdukKesehatanViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Katagori_Produk_Kesehatan prod = db.m_medical_item_category
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault(); //kembali ke element pertama jika element tidak diketemukan
                    if (prod != null)
                    {
                        prod.Name = entity.Name;

                        prod.Modified_By = entity.Modified_By;
                        prod.Modified_On = DateTime.Now;
                        prod.Is_Delete = false;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Delete(long id)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Katagori_Produk_Kesehatan prod = db.m_medical_item_category
                        .Where(o => o.Id == id)
                        .FirstOrDefault();
                    if (prod != null)
                    {
                        db.m_medical_item_category.Remove(prod);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }






    }
}
