﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using System.Linq;

namespace DataAccess
{
    public class BiodataRepo
    {
        public static List<BiodataViewModel> All()
        {
            List<BiodataViewModel> result = new List<BiodataViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from o in db.biodatas
                          select new BiodataViewModel
                          {
                              Id = o.Id,
                              FullName = o.FullName,
                              MobilePhone = o.MobilePhone,
                              Image_Path = o.Image_Path
                          }).ToList();
            }
            return result;
        }

        public static BiodataViewModel ById(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(c => c.Id == id)
                    .Select(c => new BiodataViewModel
                    {
                              Id = c.Id,
                              FullName = c.FullName,
                              MobilePhone = c.MobilePhone,
                              Image_Path = c.Image_Path
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(BiodataViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    biodata ent = new biodata();

                    ent.Image_Path = entity.Image_Path;
                    ent.FullName = entity.FullName;
                    ent.MobilePhone = entity.MobilePhone;
                    db.biodatas.Add(ent);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool Edit(BiodataViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    biodata pro = db.biodatas
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                    if (pro != null)
                    {
                        pro.Image_Path = entity.Image_Path;
                        pro.FullName = entity.FullName;
                        pro.MobilePhone = entity.MobilePhone;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;

        }
    }
}