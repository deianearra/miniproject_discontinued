﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using DataModel;
using System.Linq;

namespace DataAccess
{
    public class Wallet_Default_NominalRepo
    {
        public static List<NominalDompetElektronikViewModel> All()
        {
            List<NominalDompetElektronikViewModel> result = new List<NominalDompetElektronikViewModel>();
            using (var db = new XsisPosDbContext())
            {

                result = (from c in db.Wallet_Default_Nominals
                          select new NominalDompetElektronikViewModel
                          {
                              Id = c.Id,
                              Nominal = c.Nominal,
                              Is_Delete = false
                          }).ToList();
            }
            return result;
        }

        public static NominalDompetElektronikViewModel ById(long id)
        {
            NominalDompetElektronikViewModel result = new NominalDompetElektronikViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Wallet_Default_Nominals
                    .Where(o => o.Id == id)
                    .Select(o => new NominalDompetElektronikViewModel
                    {
                        Id = o.Id,
                        Nominal = o.Nominal,
                        Created_By = o.Created_By,
                        Created_On = o.Created_On,
                        Modified_By = o.Modified_By,
                        Modified_On = (DateTime)o.Modified_On,
                        Is_Delete = o.Is_Deleted
                    }).FirstOrDefault();
            }
            return result;
        }
        public static bool Create(NominalDompetElektronikViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Wallet_Default_Nominal nom = new Wallet_Default_Nominal();
                    nom.Id = nom.Id;
                    nom.Nominal = entity.Nominal;
                    nom.Created_By = 1;
                    nom.Created_On = DateTime.Now;
                    nom.Is_Deleted = false;

                    db.Wallet_Default_Nominals.Add(nom);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool Delete(long id)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Wallet_Default_Nominal prod = db.Wallet_Default_Nominals
                        .Where(o => o.Id == id)
                        .FirstOrDefault();
                    if (prod != null)
                    {
                        db.Wallet_Default_Nominals.Remove(prod);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
    }
}
