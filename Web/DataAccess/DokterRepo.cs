﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using DataModel;
using System.Linq;

namespace DataAccess
{
    public class DokterRepo
    {

        public static List<MenuViewModel> All(long id)
        {
            List<MenuViewModel> result = new List<MenuViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from m in db.m_menu.Where(m => m.Parent_Id == id && m.Id != id)
                          select new MenuViewModel
                          {
                              Id = m.Id,
                              Name = menuUrl(m.Url),
                              Url = m.Url,
                              Parent_Id = (int)m.Parent_Id,
                              Big_Icon = m.Big_Icon,
                              Small_Icon = m.Small_Icon,
                              Is_Deleted = m.Is_Deleted
                          }).ToList();
            }
            return result;
        }
        public static bool Find(long id)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                Doctor doc = db.Doctors
                    .Where(u => u.Biodata_Id == id)
                    .FirstOrDefault();
                if (doc != null)
                {
                    result = true;
                }
            }
            return result;
        }
        public static string menuUrl(string menuname)
        {
            string result = "";
            string[] menu = menuname.Split('_');
            result = String.Join(' ', menu);
            return result;
        }

        public static DokterProfilViewModel GetDokter(long id)
        {
            DokterProfilViewModel result = new DokterProfilViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctors
                    .Where(b => b.Id == id)
                    .Select(b => new DokterProfilViewModel
                    {
                        Id = b.Id,
                        Biodata_Id = b.Biodata_Id,
                        Str = b.Str
                    }).FirstOrDefault();
            }
            return result;
        }
        public static BiodataViewModel GetBiodata(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(b => b.Id == id)
                    .Select(b => new BiodataViewModel
                    {
                        Id = b.Id,
                        FullName = b.FullName,
                        MobilePhone = b.MobilePhone
                    }).FirstOrDefault();
            }
            return result;
        }
        public static bool EditFoto(BiodataViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    biodata pro = db.biodatas
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                    if (pro != null)
                    {
                        pro.Image_Path = entity.Image_Path;
                        pro.FullName = entity.FullName;
                        pro.MobilePhone = entity.MobilePhone;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;

        }
        public static DokterProfilViewModel ById(long id)
        {
            DokterProfilViewModel result = new DokterProfilViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctors
                    .Where(u => u.Id == id)
                    .Select(u => new DokterProfilViewModel
                    {
                        Id = u.Id,
                        Biodata_Id = u.Biodata_Id,
                        Created_By = u.Created_By,
                        Created_On = u.Created_On,
                        Modified_By = u.Modified_By,
                        Modified_On = (DateTime)u.Modified_On,
                        Deleted_By = u.Deleted_By,
                        Deleted_On = (DateTime)u.Deleted_On,
                        Is_Delete = u.Is_Delete
                    }).FirstOrDefault();
            }
            return result;
        }
        public static BiodataViewModel ByIdBio(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(c => c.Id == id)
                    .Select(c => new BiodataViewModel
                    {
                        Id = c.Id,
                        FullName = c.FullName,
                        MobilePhone = c.MobilePhone,
                        Image_Path = c.Image_Path
                    }).FirstOrDefault();
            }
            return result;
        }
        public static DoctorEducationViewModel GetDoctorEducation(long id)
        {
            DoctorEducationViewModel result = new DoctorEducationViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctor_Educations
                    .Where(b => b.Id == id)
                    .Select(b => new DoctorEducationViewModel
                    {
                        Id = b.Id,
                        Doctor_Id = b.Doctor_Id,
                        Education_Level_Id = b.Education_Level_Id,
                        Institution_Name = b.Institution_Name,
                        Major = b.Major,
                        Start_year = b.Start_year,
                        End_year = b.End_year,
                        Is_Last_Education = b.Is_Last_Education
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(DokterProfilViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Doctor doc = new Doctor();


                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
    }
}
