﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace DataAccess
{
    public class GolonganDarahRepo
    {
        public static List<GolonganDarahViewModel> All()
        {
            List<GolonganDarahViewModel> result = new List<GolonganDarahViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from c in db.m_blood_group
                          select new GolonganDarahViewModel
                          {
                              Id = c.Id,
                              Code = c.Code,
                              Description = c.Description,
                              Created_by = c.Created_By,
                              Created_on = c.Created_On,
                              Modified_by = c.Modified_By,
                              Modified_on = c.Modified_On,
                              Deleted_by = c.Deleted_by,
                              Deleted_on = c.Deleted_On,
                              Is_delete = c.Is_Delete
                          }).ToList();
            }
            return result;
        }
        public static Tuple<List<GolonganDarahViewModel>, int> All(int page, int rows, string search)
        {
            List<GolonganDarahViewModel> result = new List<GolonganDarahViewModel>();

            int rowNum = 0;
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_blood_group.Where(o => (o.Code.Contains(search) || o.Description.Contains(search)) && o.Is_Delete == false);
                rowNum = query.Count();
                result = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                    .Select(c => new GolonganDarahViewModel
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Description = c.Description,
                        Is_delete = c.Is_Delete
                    }
                    ).ToList();
            }
            return new Tuple<List<GolonganDarahViewModel>, int>(result, rowNum); //Tuple adalah pengembalian lebih dari satu data
        }

        public static bool UpdateDeleted(GolonganDarahViewModel entity)
        {
            bool result = true;
            using (var db = new XsisPosDbContext())
            {
                Golongan_Darah golDar = db.m_blood_group
                    .Where(o => o.Code == entity.Code && o.Is_Delete == true)
                    .FirstOrDefault();
                if (golDar != null)
                {
                    golDar.Code = entity.Code;
                    golDar.Description = entity.Description;
                    golDar.Created_By = entity.Created_by;
                    golDar.Created_On = DateTime.Now;
                    golDar.Is_Delete = false;

                    db.m_blood_group.Add(golDar);
                    db.SaveChanges();
                }
            }
            return result;
        }
        public static bool Find(string name)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                Golongan_Darah golDar = db.m_blood_group
                    .Where(u => u.Code == name)
                    .FirstOrDefault();
                if (golDar != null)
                {
                    result = true;
                }
            }
            return result;
        }
        public static GolonganDarahViewModel ById(long id)
        {
            GolonganDarahViewModel result = new GolonganDarahViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_blood_group
                    .Where(c => c.Id == id)
                    .Select(c => new GolonganDarahViewModel
                    {
                        Id = c.Id,
                        Code = c.Code,
                        Description = c.Description
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(GolonganDarahViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Golongan_Darah golDar = new Golongan_Darah();
                    golDar.Id = entity.Id;
                    golDar.Code = entity.Code;
                    golDar.Description = entity.Description;

                    golDar.Created_By = entity.Created_by;
                    golDar.Created_On = DateTime.Now;

                    db.m_blood_group.Add(golDar);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Edit(GolonganDarahViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Golongan_Darah golDar = db.m_blood_group
                        .Where(c => c.Id == entity.Id)
                        .FirstOrDefault();
                    if (golDar != null)
                    {
                        golDar.Id = entity.Id;
                        golDar.Code = entity.Code;
                        golDar.Description = entity.Description;

                        golDar.Modified_By = entity.Modified_by;
                        golDar.Modified_On = DateTime.Now;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Delete(GolonganDarahViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Golongan_Darah golDar = db.m_blood_group
                        .Where(c => c.Id == entity.Id)
                        .FirstOrDefault();

                    if (golDar != null)
                    {
                        golDar.Code = null;
                        golDar.Deleted_by = entity.Deleted_by;
                        golDar.Deleted_On = DateTime.Now;
                        golDar.Is_Delete = true;
                        //db.m_blood_group.Remove(golDar);
                        db.SaveChanges();

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        //public static List<GolonganDarahViewModel> CantDelete()
        //{
        //    List<GolonganDarahViewModel> result = new List<GolonganDarahViewModel>();
        //    using (var db = new XsisPosDbContext())
        //    {
        //        var query = db.m_blood_group
        //       .Where(a => (a.Is_Delete == false));
        //        result = query
        //                   .Select(a => new GolonganDarahViewModel
        //                   {
        //                       location_level_id = (long)a.location_level_id,
        //                   }).ToList();
        //    }
        //    return result;
        //}

        //public static List<LocationLevelViewModel> CantCreate()
        //{
        //    List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
        //    using (var db = new XsisPosDbContext())
        //    {
        //        var query = db.m_location_level
        //       .Where(a => (a.Is_deleted == false));
        //        result = query
        //                   .Select(a => new LocationLevelViewModel
        //                   {
        //                       Name = a.Name,
        //                   }).ToList();
        //    }
        //    return result;
        //}
    }
}
