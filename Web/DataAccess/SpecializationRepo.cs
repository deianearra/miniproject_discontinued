﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using DataModel;

namespace DataAccess
{
    public class SpecializationRepo
    {
        public static List<SpecializationViewModel> All()
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            using (var db = new XsisPosDbContext())
            {

                result = (from c in db.m_specialization
                          select new SpecializationViewModel
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return result;
        }

        public static Tuple<List<SpecializationViewModel>, int> All(int page, int rows, string search)
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            int rowNum = 0;
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_specialization.Where(o => o.Name.Contains(search) && o.Is_Deleted == false);
                rowNum = query.Count();
                result = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                    .Select(s => new SpecializationViewModel
                    {
                        Id = s.Id,
                        Name = s.Name,
                        Created_By = s.Created_By,
                        Created_On = s.Created_On,
                        Modified_By = s.Modified_By,
                        Modified_On = (DateTime)s.Modified_On,
                        Deleted_By = s.Deleted_By,
                        Deleted_On = (DateTime)s.Deleted_On,
                        Is_Delete = s.Is_Deleted
                    }).ToList();
            }
            return new Tuple<List<SpecializationViewModel>, int>(result, rowNum); //Tuple adalah pengembalian lebih dari satu data
        }

        public static List<SpecializationViewModel> ByFilter(string search)
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_specialization
                    .Where(o => o.Name.Contains(search));

                result = query.Take(3)
                          .Select(o => new SpecializationViewModel
                          {
                              Id = o.Id,
                              Name = o.Name,
                              Created_By = o.Created_By,
                              Created_On = (DateTime)o.Created_On,
                              Modified_By = o.Modified_By,
                              Modified_On = (DateTime)o.Modified_On,
                              Deleted_By = o.Deleted_By,
                              Deleted_On = (DateTime)o.Deleted_On,
                              Is_Delete = o.Is_Deleted

                          }).ToList();
            }
            return result;
        }


        public static SpecializationViewModel ById(long id)
        {
            SpecializationViewModel result = new SpecializationViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_specialization
                    .Where(o => o.Id == id)
                    .Select(o => new SpecializationViewModel
                    {
                        Id = o.Id,
                        Name = o.Name,
                        Created_By = o.Created_By,
                        Created_On= o.Created_On,
                        Modified_By= o.Modified_By,
                        Modified_On= (DateTime)o.Modified_On,
                        Deleted_By= o.Deleted_By,
                        Deleted_On= (DateTime)o.Deleted_On,
                        Is_Delete= o.Is_Deleted
                    }).FirstOrDefault();
            }
            return result;
        }
        public static bool Find(string name)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                Specialization spe = db.m_specialization
                    .Where(u => u.Name == name)
                    .FirstOrDefault();
                if (spe != null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool Create(SpecializationViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Specialization spe = new Specialization();

                    spe.Name = entity.Name;
                    spe.Created_By = entity.Created_By;
                    spe.Created_On = DateTime.Now;
                    spe.Is_Deleted = false;

                    db.m_specialization.Add(spe);
                    db.SaveChanges();
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool Edit(SpecializationViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Specialization spe = db.m_specialization
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                    if (spe != null)
                    {
                        spe.Name = entity.Name;
                        spe.Modified_By = entity.Modified_By;
                        spe.Modified_On = DateTime.Now;
                        spe.Is_Deleted= false;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool UpdateDeleted(SpecializationViewModel entity)
        {
            bool result = true;
            using (var db = new XsisPosDbContext())
            {
                Specialization spe = db.m_specialization
                    .Where(o => o.Name == entity.Name)
                    .FirstOrDefault(); //kembali ke element pertama jika element tidak diketemukan
                if (spe != null)
                {
                    spe.Created_By = entity.Created_By;
                    spe.Created_On = DateTime.Now;
                    spe.Is_Deleted = false;

                    db.SaveChanges();
                }
            }
            return result;
        }

        public static bool Delete(SpecializationViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Specialization spe = db.m_specialization
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();

                    if (spe != null)
                    {
                        spe.Deleted_By = entity.Deleted_By;
                        spe.Deleted_On = DateTime.Now;
                        spe.Is_Deleted = true;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

    }
}
