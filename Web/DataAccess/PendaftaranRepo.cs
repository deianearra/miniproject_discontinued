﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class PendaftaranRepo
    {
        public static List<BioDaftarViewModel> All()
        {
            List<BioDaftarViewModel> result = new List<BioDaftarViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.biodatas
                    .Where(a => (a.Is_Deleted == false));
                result = query
                           .Select(a => new BioDaftarViewModel
                           {
                               Id = a.Id,
                               FullName = a.FullName,
                               MobilePhone = a.MobilePhone,
                           }).ToList();

                var forRole = db.users
                    .Where(a => (a.Is_Deleted == false));
                result = forRole
                           .Select(a => new BioDaftarViewModel
                           {
                               RoleId = a.Role_Id,
                               RoleName = a.Role.Name
                           }).ToList();
            }
            return result;
        }

        public static ResponseResult Create(PendaftaranViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user cat = new user();
                    cat.Email = entity.Email;
                    cat.Login_Attempt = 0;
                    cat.Is_Locked = false;

                    cat.Created_By = 0;
                    cat.Created_On = DateTime.Now;

                    db.users.Add(cat);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Edit(SetPasswordViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user vrt = db.users
                        .Where(o => o.Password == null)
                        .FirstOrDefault();

                    if (vrt != null)
                    {
                        vrt.Password = entity.Password;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public static ResponseResult Insert(BioDaftarViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            using var db = new XsisPosDbContext();
            using var transaction = db.Database.BeginTransaction();

            try
            {
                biodata cat = new biodata();
                cat.FullName = entity.FullName;
                cat.MobilePhone = entity.MobilePhone;

                cat.Created_By = 0;
                cat.Created_On = DateTime.Now;

                db.biodatas.Add(cat);
                db.SaveChanges();

                biodata bio = db.biodatas
                    .OrderByDescending(o => o.Id)
                    .FirstOrDefault();
                if (cat.Created_By == 0)
                {
                    cat.Created_By = Convert.ToInt32(bio.Id);

                    db.SaveChanges();
                }

                user loc = db.users
                    .Where(o => o.Role_Id == null)
                    .FirstOrDefault();
                if (loc != null)
                {
                    loc.Biodata_Id = bio.Id;
                    loc.Created_By = Convert.ToInt32(bio.Id);
                    loc.Role_Id = entity.RoleId;

                    db.SaveChanges();
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
