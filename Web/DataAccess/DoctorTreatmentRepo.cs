﻿using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;
using DataModel;
using System.Text;

namespace DataAccess
{
    public class DoctorTreatmentRepo
    {
        public static List<DoctorTreatmentViewModel> All()
        {
            List<DoctorTreatmentViewModel> result = new List<DoctorTreatmentViewModel>();
            using (var tr = new XsisPosDbContext())
            {
                result = (from d in tr.t_doctor_treatment
                          select new DoctorTreatmentViewModel
                          {
                              Id = d.Id,
                              Doctor_Id = d.Doctor_Id,
                              Name = d.Name,
                              Is_Delete = false
                          }).ToList();
            }
            return result;
        }

        public static DoctorTreatmentViewModel ById(long id)
        {
            DoctorTreatmentViewModel result = new DoctorTreatmentViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.t_doctor_treatment
                    .Where(o => o.Id == id)
                    .Select(o => new DoctorTreatmentViewModel
                    {
                        Id = o.Id,
                        Doctor_Id = o.Doctor_Id,
                        Name = o.Name,
                        Created_By = o.Created_By,
                        Created_On = o.Created_On,
                        Modified_By = o.Modified_By,
                        Modified_On = (DateTime)o.Modified_On,
                        Is_Delete = o.Is_Delete
                    }).FirstOrDefault();
            }
            return result;
        }
        public static bool Find(string name)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                Doctor_Treatment spe = db.t_doctor_treatment
                    .Where(u => u.Name == name)
                    .FirstOrDefault();
                if (spe != null)
                {
                    result = true;
                }
            }
            return result;
        }
        public static bool Create(DoctorTreatmentViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Doctor_Treatment doc = new Doctor_Treatment();
                    doc.Doctor_Id = doc.Doctor_Id;
                    doc.Name = entity.Name;
                    doc.Created_By = 1;
                    doc.Created_On = DateTime.Now;
                    doc.Is_Delete = false;

                    db.t_doctor_treatment.Add(doc);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
        public static bool UpdateDeleted(DoctorTreatmentViewModel entity)
        {
            bool result = true;
            using (var db = new XsisPosDbContext())
            {
                Doctor_Treatment tre = db.t_doctor_treatment
                    .Where(o => o.Name == entity.Name)
                    .FirstOrDefault(); //kembali ke element pertama jika element tidak diketemukan
                if (tre != null)
                {
                    tre.Created_By = entity.Created_By;
                    tre.Created_On = DateTime.Now;
                    tre.Is_Delete = false;

                    db.SaveChanges();
                }
            }
            return result;
        }
        public static bool Delete(DoctorTreatmentViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Doctor_Treatment tre = db.t_doctor_treatment
                        .Where(o => o.Id == entity.Id)
                        .FirstOrDefault();
                    if (tre != null)
                    {
                        tre.Deleted_By = entity.Deleted_By;
                        tre.Deleted_On = DateTime.Now;
                        tre.Is_Delete = true;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e) //untuk error handling jika terjadi sesuatu saat menyimpan (Jika ada masalah di Try akan masuk ke Catch)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

    }
}
