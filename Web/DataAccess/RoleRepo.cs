﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class RoleRepo
    {
        public static List<RoleViewModel> All()
        {
            List<RoleViewModel> result = new List<RoleViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.roles
                    .Where(a => (a.Is_Deleted == false));
                result = query
                           .Select(a => new RoleViewModel
                           {
                               Id = a.Id,
                               Name = a.Name,
                               Code = a.Code,
                           }).ToList();
            }
            return result;
        }
    }
}
