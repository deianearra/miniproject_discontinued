﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace DataAccess
{
    public class LocationLevelRepo
    {
        public static List<LocationLevelViewModel> All()
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_location_level
               .Where(a => (a.Is_deleted == false));
                result = query
                           .Select(a => new LocationLevelViewModel
                           {
                               Id = a.Id,
                               Name = a.Name,
                               Abbreviation = a.Abbreviation,
                               Created_by = a.Created_by,
                               Created_on = a.Created_on,
                               Modified_by = a.Modified_by,
                               Deleted_by = a.Deleted_by,
                               Deleted_on = a.Deleted_on,
                               Is_deleted = a.Is_deleted
                           }).ToList();
            }
            return result;
        }

        public static Tuple<List<LocationLevelViewModel>, int> All(int page, int rows, string search)
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();

            int rowNum = 0;
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_location_level.Where(o => (o.Name.Contains(search) || o.Abbreviation.Contains(search)) && o.Is_deleted == false);
                rowNum = query.Count();
                result = query
                    .Skip((page - 1) * rows)
                    .Take(rows)
                    .Select(a => new LocationLevelViewModel
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Abbreviation = a.Abbreviation,
                        Is_deleted = a.Is_deleted
                    }
                    ).ToList();
            }
            return new Tuple<List<LocationLevelViewModel>, int>(result, rowNum); //Tuple adalah pengembalian lebih dari satu data
        }

        public static List<LocationLevelViewModel> ByFilter(string search)
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_location_level
                    .Where(a => (a.Name.Contains(search) || a.Abbreviation.Contains(search)) && a.Is_deleted == false);

                result = query.Take(3)
                    .Select(a => new LocationLevelViewModel
                    {
                        Id = a.Id,
                        Name = a.Name,
                        Abbreviation = a.Abbreviation,
                    }).ToList();
            }
            return result;
        }

        public static bool UpdateDeleted(LocationLevelViewModel entity)
        {
            bool result = true;
            using (var db = new XsisPosDbContext())
            {
                LocationLevel spe = db.m_location_level
                    .Where(o => o.Name == entity.Name && o.Is_deleted == true)
                    .FirstOrDefault();
                if (spe != null)
                {
                    spe.Name = entity.Name;
                    spe.Abbreviation = entity.Abbreviation;
                    spe.Created_by = entity.Created_by;
                    spe.Created_on = DateTime.Now;
                    spe.Is_deleted = false;

                    db.m_location_level.Add(spe);
                    db.SaveChanges();
                }
            }
            return result;
        }
        public static bool Find(string name)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                LocationLevel spe = db.m_location_level
                    .Where(u => u.Name == name)
                    .FirstOrDefault();
                if (spe != null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static LocationLevelViewModel ById(long id)
        {
            LocationLevelViewModel result = new LocationLevelViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_location_level
                    .Where(o => o.Id == id)
                    .Select(o => new LocationLevelViewModel
                    {
                        Id = o.Id,
                        Name = o.Name,
                        Abbreviation = o.Abbreviation,
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(LocationLevelViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    LocationLevel loc = new LocationLevel();
                    loc.Id = entity.Id;
                    loc.Name = entity.Name;
                    loc.Abbreviation = entity.Abbreviation;
                    loc.Created_by = entity.Created_by;
                    loc.Created_on = DateTime.Now;

                    db.m_location_level.Add(loc);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        public static bool Edit(LocationLevelViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    LocationLevel loc = db.m_location_level
                         .Where(o => o.Id == entity.Id)
                         .FirstOrDefault();
                    if (loc != null)
                    {
                        loc.Name = entity.Name;
                        loc.Abbreviation = entity.Abbreviation;

                        loc.Modified_by = entity.Modified_by;
                        loc.Modified_on = DateTime.Now;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        public static bool Delete(LocationLevelViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    LocationLevel loc = db.m_location_level
                         .Where(o => o.Id == entity.Id)
                         .FirstOrDefault();
                    if (loc != null)
                    {
                        loc.Name = null;
                        loc.Deleted_by = entity.Deleted_by;
                        loc.Deleted_on = DateTime.Now;
                        loc.Is_deleted = true;

                        /* db.m_location_level.Remove(loc);*/
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result = false;
            }
            return result;
        }

        public static List<LocationViewModel> CantDelete()
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_location
               .Where(a => (a.Is_deleted == false));
                result = query
                           .Select(a => new LocationViewModel
                           {
                               Location_Level_Id = (long)a.location_level_id,
                           }).ToList();
            }
            return result;
        }

        public static List<LocationLevelViewModel> CantCreate()
        {
            List<LocationLevelViewModel> result = new List<LocationLevelViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.m_location_level
               .Where(a => (a.Is_deleted == false));
                result = query
                           .Select(a => new LocationLevelViewModel
                           {
                               Name = a.Name,
                           }).ToList();
            }
            return result;
        }



    }
}
