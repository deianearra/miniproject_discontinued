﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ViewModel;

namespace DataAccess
{
    public class ResetPasswordRepo
    {
        public static PendaftaranViewModel ByEmail(string email)
        {
            PendaftaranViewModel result = new PendaftaranViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.users
                    .Where(o => o.Email == email)
                    .Select(o => new PendaftaranViewModel
                    {
                        Id = o.Id,
                        Email = o.Email
                    }).FirstOrDefault();
            }
            return result;
        }

        public static List<PendaftaranViewModel> IsRegistered()
        {
            List<PendaftaranViewModel> result = new List<PendaftaranViewModel>();
            using (var db = new XsisPosDbContext())
            {
                var query = db.users
                    .Where(a => (a.Is_Deleted == false));
                result = query
                           .Select(a => new PendaftaranViewModel
                           {
                               Email = a.Email
                           }).ToList();
            }
            return result;
        }

        public static ResponseResult Edit(SetPasswordViewModel entity, string email)
        {
            
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user vrt = db.users
                        .Where(o => o.Email == email)
                        .FirstOrDefault();

                    if (vrt != null)
                    {
                        vrt.Password = entity.Password;
                        vrt.Modified_By = Convert.ToInt32(vrt.Biodata_Id);
                        vrt.Modified_On = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }
    }
}
