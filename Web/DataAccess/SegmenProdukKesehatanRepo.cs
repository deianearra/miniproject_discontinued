﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using ViewModel;

namespace DataAccess
{
    public class SegmenProdukKesehatanRepo
    {
        public static List<SegmenProdukKesehatanViewModel> All()
        {
            List<SegmenProdukKesehatanViewModel> result = new List<SegmenProdukKesehatanViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from c in db.m_medical_item_segmentation
                          select new SegmenProdukKesehatanViewModel
                          {
                              Id = c.Id,
                              Name = c.Name
                          }).ToList();
            }
            return result;
        }

        public static SegmenProdukKesehatanViewModel ById(long id)
        {
            SegmenProdukKesehatanViewModel result = new SegmenProdukKesehatanViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_medical_item_segmentation
                    .Where(c => c.Id == id)
                    .Select(c => new SegmenProdukKesehatanViewModel
                    {
                        Id = c.Id,
                        Name = c.Name
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(SegmenProdukKesehatanViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Segmen_Produk_Kesehatan segmen = new Segmen_Produk_Kesehatan();
                    segmen.Id = entity.Id;
                    segmen.Name = entity.Name;

                    segmen.Created_By = 0;
                    segmen.Created_On = DateTime.Now;

                    db.m_medical_item_segmentation.Add(segmen);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Edit(SegmenProdukKesehatanViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Segmen_Produk_Kesehatan segmen = db.m_medical_item_segmentation
                        .Where(c => c.Id == entity.Id)
                        .FirstOrDefault();
                    if (segmen != null)
                    {
                        segmen.Id = entity.Id;
                        segmen.Name = entity.Name;

                        segmen.Modified_By = 0;
                        segmen.Modified_On = DateTime.Now;

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool Delete(long id)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Segmen_Produk_Kesehatan segmen = db.m_medical_item_segmentation
                        .Where(c => c.Id == id)
                        .FirstOrDefault();

                    if (segmen != null)
                    {
                        db.m_medical_item_segmentation.Remove(segmen);
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
    }
}
