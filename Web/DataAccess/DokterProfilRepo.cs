﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using DataModel;
using System.Linq;

namespace DataAccess
{
    public class DokterProfilRepo
    {
        
        public static DokterProfilViewModel GetDokter(long id)
        {
            DokterProfilViewModel result = new DokterProfilViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctors
                    .Where(b => b.Id == id)
                    .Select(b => new DokterProfilViewModel
                    {
                        Id = b.Id,
                        Biodata_Id=b.Biodata_Id,
                        Str = b.Str
                    }).FirstOrDefault();
            }
            return result;
        }
        public static BiodataViewModel GetBiodata(long id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(b => b.Id == id)
                    .Select(b => new BiodataViewModel
                    {
                        Id = b.Id,
                        FullName = b.FullName,
                        MobilePhone = b.MobilePhone
                    }).FirstOrDefault();
            }
            return result;
        }

        public static DokterProfilViewModel ById(long id)
        {
            DokterProfilViewModel result = new DokterProfilViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctors
                    .Where(u => u.Id == id)
                    .Select(u => new DokterProfilViewModel
                    {
                        Id = u.Id,
                        Biodata_Id=u.Biodata_Id
                    }).FirstOrDefault();
            }
            return result;
        }
        
        public static DoctorEducationViewModel GetDoctorEducation(long id)
        {
            DoctorEducationViewModel result = new DoctorEducationViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.Doctor_Educations
                    .Where(b => b.Id == id)
                    .Select(b => new DoctorEducationViewModel
                    {
                        Id = b.Id,
                        Doctor_Id = b.Doctor_Id,
                        Education_Level_Id = b.Education_Level_Id,
                        Institution_Name = b.Institution_Name,
                        Major = b.Major,
                        Start_year = b.Start_year,
                        End_year = b.End_year,
                        Is_Last_Education = b.Is_Last_Education
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Create(DokterProfilViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Doctor doc = new Doctor();


                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }
    }
}
