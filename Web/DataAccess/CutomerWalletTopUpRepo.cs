﻿using System;
using System.Collections.Generic;
using System.Text;
using ViewModel;
using DataModel;

namespace DataAccess
{
    public class CutomerWalletTopUpRepo
    {
        public static bool IsiSaldo(CustomerWalletTopUpViewModel entity)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    Customer_Wallet_Top_Up wal = new Customer_Wallet_Top_Up();
                    wal.Id = entity.Id;
                    wal.Amount = wal.Amount + entity.Amount;
                    wal.Created_By = 1;
                    wal.Created_On = DateTime.Now;

                    db.Customer_Wallet_Top_Ups.Add(wal);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

    }
}
