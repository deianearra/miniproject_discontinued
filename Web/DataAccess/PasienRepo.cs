﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ViewModel;

namespace DataAccess
{
    public class PasienRepo
    {
        public static List<MenuViewModel> All(int id)
        {
            List<MenuViewModel> result = new List<MenuViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from m in db.m_menu.Where(m => m.Parent_Id == id && m.Id != id)
                          select new MenuViewModel
                          {
                              Id = m.Id,
                              Name = m.Name,
                              Url = m.Url,
                              Btn_Id = btnId(m.Url),
                              Parent_Id = (int)m.Parent_Id,
                              Big_Icon = m.Big_Icon,
                              Small_Icon = m.Small_Icon,
                              Is_Deleted = m.Is_Deleted
                          }).ToList();
            }
            return result;
        }
        public static string btnId(string url)
        {
            string result = "";
            string[] menu = url.Split('_');
            result = String.Join('-', menu);
            return result;
        }
        public static List<LocationViewModel> LocationCariDokter()
        {
            List<LocationViewModel> result = new List<LocationViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from l in db.m_location.Where(l => l.location_level_id == 4).OrderBy(l => l.Name)
                          select new LocationViewModel
                          {
                              Id = l.Id,
                              Name = l.Name,
                              Location_Level_Id = (int)l.location_level_id,
                              Parent_Id = (int)l.parent_id
                          }).ToList();
            }
            return result;
        }
        public static List<SpecializationViewModel> SpecializationCariDokter()
        {
            List<SpecializationViewModel> result = new List<SpecializationViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from s in db.m_specialization.OrderBy(s => s.Name)
                          select new SpecializationViewModel
                          {
                              Id = s.Id,
                              Name = s.Name,
                          }).ToList();
            }
            return result;
        }
        public static List<DoctorTreatmentViewModel> TreatmentCariDokter()
        {
            List<DoctorTreatmentViewModel> result = new List<DoctorTreatmentViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (from t in db.t_doctor_treatment.OrderBy(t => t.Name)
                          select new DoctorTreatmentViewModel
                          {
                              Id = t.Id,
                              Doctor_Id = (int)t.Doctor_Id,
                              Name = t.Name
                          }).ToList();
            }
            return result;
        }

        public static List<ListDokterViewModel> GetListDokter(CariDokterViewModel model)
        {
            // loct: cengkareng, name:dokter, spec: gigi, treatment: mri otak
            List<ListDokterViewModel> result = new List<ListDokterViewModel>();
            using (var db = new XsisPosDbContext())
            {
                result = (
                    // berdasarkan nama berjalan lancar
                    from doc in db.Doctors
                    join bio in db.biodatas
                        .Where(bio => bio.FullName
                        .Contains(model.Doctor_Name))
                    on doc.Biodata_Id equals bio.Id

                    // berdasarkan specialization berjalan lancar
                    join currdocspec in db.t_current_Doctor_Specializations
                    /*.Where(currdocspec => currdocspec.Specialization_Id == model.Specialization_Id)*/
                    on doc.Id equals currdocspec.Doctor_Id

                    join spec in db.m_specialization on currdocspec.Specialization_Id equals spec.Id

                    // berdasarkan location berjalan lancar
                    join docoff in db.t_doctor_office on doc.Id equals docoff.Doctor_Id
                    join medfac in db.m_medical_facility
                    /*.Where(medfac => medfac.Location_Id == model.Location_Id)*/
                    on docoff.Medical_Facility_Id equals medfac.Id

                    join loc in db.m_location
                    on medfac.Location_Id equals loc.Id

                    // berdasarkan treatment
                    join doctreat in db.t_doctor_treatment
                    /*.Where(doctreat => doctreat.Id == model.Treatment_Id)*/
                    on doc.Id equals doctreat.Doctor_Id

                    // 1. mendapatkan lama pengalaman = tahun terkecil pada created_on sampai datetime.now.year

                    join docoffsche in db.t_doctor_office_schedule
                    on doc.Id equals docoffsche.Doctor_Id

                    select new ListDokterViewModel
                    {
                        FullName = bio.FullName,
                        Specialization_Name = spec.Name,
                        Doctor_Experience = DateTime.Now.Year - docoffsche.Created_On.Year,
                        Med_Fac_Name = medfac.Name,
                        Location_Name = loc.Name,
                        Image_Path = bio.Image_Path,
                        Treatment_Name = doctreat.Name,
                    }).ToList();
            }
            return result;
        }

        public static string GeKeywordLoct(int id)
        {
            string keyword = "";
            using (var db = new XsisPosDbContext())
            {
                keyword = db.m_location
                    .Where(l => l.Id == id)
                    .Select(l => l.Name)
                    .FirstOrDefault();
            }
            return keyword;
        }
    }
}
