﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ViewModel;

namespace DataAccess
{
    public class LoginRepo
    {
        public static ResponseResult Response(UserViewModel user)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                bool emailFound = CheckEmail(user.Email);
                if (emailFound == true)
                {
                    var userGet = GetUser(user.Email);
                    if (userGet.Is_Deleted == false)
                    {
                        if (userGet.Is_Locked == false)
                        {
                            bool password = CheckPassword(userGet.Email, user.Password);
                            if (password == true)
                            {
                                if (userGet.Login_Attempt < 3)
                                {
                                    RefreshAttempt(userGet.Email);
                                    UpdateLastLogin(userGet.Email);
                                    result.Message = "Login Successful!";
                                }
                                else // jika percobaan login >= 3
                                {
                                    UpdateLocked(userGet.Email);
                                    result.Message = "Account has been locked! Please Contact Admin!";
                                    result.Success = false;
                                }
                            }
                            else // jika password salah
                            {
                                UpdateAttempt(userGet.Email, userGet.Login_Attempt);
                                int newAttempt = GetAttempt(userGet.Email);
                                if (newAttempt < 3)
                                {
                                    result.Message = "Email and Password are not correct!";
                                    result.Success = false;
                                }
                                else if (newAttempt >= 3)
                                {
                                    UpdateLocked(userGet.Email);
                                    result.Message = "Account has been locked! Please Contact Admin!";
                                    result.Success = false;
                                }
                            }
                        }
                        else // jika akun terkunci
                        {
                            result.Message = "Account has been locked! Please Contact Admin!";
                            result.Success = false;
                        }
                    }
                    else // jika akun dihapus
                    {
                        result.Message = "Account has been deleted!";
                        result.Success = false;
                    }
                }
                else // jika email tidak ketemu
                {
                    result.Message = "Account not found";
                    result.Success = false;
                }
            }
            catch (Exception e)
            {
                result.Message = e.Message;
                result.Success = false;
            }
            return result;
        }

        public static bool CheckEmail(string email)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                user user = db.users
                    .Where(u => u.Email == email)
                    .FirstOrDefault();
                if (user != null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static UserViewModel GetUser(string email)
        {
            UserViewModel result = new UserViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.users
                    .Where(u => u.Email == email)
                    .Select(u => new UserViewModel
                    {
                        Id = u.Id,
                        Biodata_Id = (int)u.Biodata_Id,
                        Role_Id = (int)u.Role_Id,
                        Email = u.Email,
                        Password = u.Password,
                        Login_Attempt = (int)u.Login_Attempt,
                        Is_Locked = (bool)u.Is_Locked,
                        Last_Login = (DateTime)u.Last_Login,
                        Is_Deleted = (bool)u.Is_Deleted
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool CheckPassword(string email, string password)
        {
            bool result = false;
            using (var db = new XsisPosDbContext())
            {
                user user = db.users
                    .Where(u => u.Email == email && u.Password == password)
                    .FirstOrDefault();
                if (user != null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool RefreshAttempt(string email)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user user = db.users
                        .Where(u => u.Email == email)
                        .FirstOrDefault();
                    if (user != null)
                    {
                        user.Login_Attempt = 0;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool UpdateLastLogin(string email)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user user = db.users
                        .Where(u => u.Email == email)
                        .FirstOrDefault();
                    if (user != null)
                    {
                        user.Last_Login = DateTime.Now;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static RoleViewModel GetRole(long role_id)
        {
            RoleViewModel result = new RoleViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.roles
                    .Where(r => r.Id == role_id)
                    .Select(r => new RoleViewModel
                    {
                        Id = r.Id,
                        Name = r.Name,
                        Code = r.Code
                    }).FirstOrDefault();
            }
            return result;
        }

        public static MenuRoleViewModel GetMenuRole(long role_Id)
        {
            MenuRoleViewModel result = new MenuRoleViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.menu_Roles
                    .Where(m => m.Role_Id == role_Id)
                    .Select(m => new MenuRoleViewModel
                    {
                        Id = m.Id,
                        Menu_Id = (int)m.Menu_Id,
                        Role_Id = (int)m.Role_Id
                    }).FirstOrDefault();
            }
            return result;
        }

        public static MenuViewModel GetMenu(long menu_Id)
        {
            MenuViewModel result = new MenuViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.m_menu
                    .Where(m => m.Id == menu_Id)
                    .Select(m => new MenuViewModel
                    {
                        Id = m.Id,
                        Name = m.Name,
                        Url = m.Url,
                        Parent_Id = (int)m.Parent_Id,
                        Big_Icon = m.Big_Icon,
                        Small_Icon = m.Small_Icon
                    }).FirstOrDefault();
            }
            return result;
        }

        public static BiodataViewModel GetBiodata(long biodata_Id)
        {
            BiodataViewModel result = new BiodataViewModel();
            using (var db = new XsisPosDbContext())
            {
                result = db.biodatas
                    .Where(b => b.Id == biodata_Id)
                    .Select(b => new BiodataViewModel
                    {
                        Id = b.Id,
                        FullName = b.FullName,
                        MobilePhone = b.MobilePhone
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool UpdateLocked(string email)
        {
            bool result = true;
            try
            {
                using (var db = new XsisPosDbContext())
                {
                    user user = db.users
                        .Where(u => u.Email == email)
                        .FirstOrDefault();
                    if (user != null)
                    {
                        user.Is_Locked = true;
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                result = false;
            }
            return result;
        }

        public static bool UpdateAttempt(string email, int attempt)
        {
            {
                bool result = true;
                try
                {
                    using (var db = new XsisPosDbContext())
                    {
                        user user = db.users
                            .Where(u => u.Email == email)
                            .FirstOrDefault();
                        if (user != null)
                        {
                            user.Login_Attempt++;
                            db.SaveChanges();
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    result = false;
                }
                return result;
            }
        }

        public static int GetAttempt(string email)
        {
            int result = 0;
            using (var db = new XsisPosDbContext())
            {
                result = db.users
                    .Where(u => u.Email == email)
                    .Select(u => (int)u.Login_Attempt)
                    .FirstOrDefault();
            }
            return result;
        }

    }
}
