﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MiniProject.Controllers
{
    public class FaskesController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
