﻿using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MiniProject.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using ViewModel;

namespace MiniProject.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View("Index");
        }

        public class argumentData
        {
            public static string Digits = string.Empty;
            public static string GetId = string.Empty;
            public static string Email = string.Empty;
        }

        #region PendaftaranController
        public IActionResult Daftar()
        {
            return PartialView("_Daftar");
        }

        [HttpPost]
        public IActionResult Daftar(PendaftaranViewModel model)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(model.Email.ToString());
                if (ModelState.IsValid)
                {
                    try
                    {
                        Random rand = new Random();
                        string digits = rand.Next(0, 999999).ToString("D6");
                        argumentData.Digits = digits;
                        argumentData.Email = model.Email.ToString();
                        string receiver = model.Email.ToString();
                        var senderEmail = new MailAddress("assazzainovic@gmail.com");
                        var receiverEmail = new MailAddress(receiver, "Receiver");
                        var password = "AnwalindaArsinta";
                        var sub = "OTP Code";
                        var body = digits;
                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(senderEmail.Address, password)
                        };
                        using (var mess = new MailMessage(senderEmail, receiverEmail)
                        {
                            Subject = sub,
                            Body = body
                        })
                        {
                            smtp.Send(mess);
                        }
                    }
                    catch (Exception)
                    {
                        ViewBag.Error = "Some Error";
                    }

                    ResponseResult result = PendaftaranRepo.Create(model);
                    if (result.Success)
                    {
                        ModelState.Clear();
                        return PartialView("_VerifyOTP");
                    }
                }
            }
            catch (Exception)
            {
                ViewBag.Error = "Some Error";
            }
            return PartialView("_Daftar", model);
        }

        public IActionResult VerifyOTP()
        {
            return PartialView("_VerifyOTP");
        }

        [HttpPost]
        public IActionResult VerifyOTP(string kodeOTP)
        {
            if (argumentData.Digits == kodeOTP)
            {
                return PartialView("_SetPassWord");
            }
            return PartialView("_VerifyOTP");
        }

        public IActionResult SetPassword()
        {
            return PartialView("_SetPassWord");
        }

        [HttpPost]
        public IActionResult SetPassword(SetPasswordViewModel model, string password, string confirmPass)
        {
            if (password == confirmPass)
            {
                ResponseResult result = PendaftaranRepo.Edit(model);
                if (result.Success)
                {
                    ModelState.Clear();
                    ViewBag.RoleList = new SelectList(RoleRepo.All(), "Id", "Name");
                    return PartialView("_BioDaftar");
                }
            }
            return PartialView("_SetPassWord");
        }

        public IActionResult BioDaftar()
        {
            ViewBag.RoleList = new SelectList(RoleRepo.All(), "Id", "Name");
            return PartialView("_BioDaftar");
        }

        [HttpPost]
        public IActionResult BioDaftar(BioDaftarViewModel model)
        {
            ResponseResult result = PendaftaranRepo.Insert(model);
            if (result.Success)
            {
                ModelState.Clear();
                return Ok(result);
            }
            return PartialView("_BioDaftar");
        }
        #endregion

        #region ReSend OTP
        public IActionResult ReSend()
        {
            Random rand = new Random();
            string digits = rand.Next(0, 999999).ToString("D6");
            argumentData.Digits = digits;
            string receiver = argumentData.Email;
            var senderEmail = new MailAddress("assazzainovic@gmail.com");
            var receiverEmail = new MailAddress(receiver, "Receiver");
            var password = "AnwalindaArsinta";
            var sub = "OTP Code";
            var body = digits;
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(senderEmail.Address, password)
            };
            using (var mess = new MailMessage(senderEmail, receiverEmail)
            {
                Subject = sub,
                Body = body
            })
            {
                smtp.Send(mess);
            }
            return Ok();
        }
        #endregion

        #region ResetPasswordController
        public IActionResult CheckEmailForget()
        {
            return PartialView("_CheckEmail");
        }

        [HttpPost]
        public IActionResult CheckEmailForget(PendaftaranViewModel model)
        {
            //PendaftaranViewModel model = ResetPasswordRepo.ByEmail(Email); Ini untuk mengambil model saat mengubah password
            ViewBag.ResetFor = "Lupa Password";
            List<PendaftaranViewModel> EmailList = ResetPasswordRepo.IsRegistered();
            foreach (PendaftaranViewModel item in EmailList)
            {
                if (item.Email == model.Email)
                {
                    argumentData.Email = model.Email;
                    if (ModelState.IsValid)
                    {
                        try
                        {
                            Random rand = new Random();
                            string digits = rand.Next(0, 999999).ToString("D6");
                            argumentData.Digits = digits;
                            string receiver = model.Email.ToString();
                            var senderEmail = new MailAddress("assazzainovic@gmail.com");
                            var receiverEmail = new MailAddress(receiver, "Receiver");
                            var password = "AnwalindaArsinta";
                            var sub = "OTP Code";
                            var body = digits;
                            var smtp = new SmtpClient
                            {
                                Host = "smtp.gmail.com",
                                Port = 587,
                                EnableSsl = true,
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = false,
                                Credentials = new NetworkCredential(senderEmail.Address, password)
                            };
                            using (var mess = new MailMessage(senderEmail, receiverEmail)
                            {
                                Subject = sub,
                                Body = body
                            })
                            {
                                smtp.Send(mess);
                            }
                        }
                        catch (Exception)
                        {
                            ViewBag.Error = "Some Error";
                        }

                        return PartialView("_ChangePassOTP");
                    }
                }
            }
            return PartialView("_CheckEmail", model);
        }

        public IActionResult CheckEmailChange()
        {
            return PartialView("_CheckEmail");
        }

        [HttpPost]
        public IActionResult CheckEmailChange(PendaftaranViewModel model)
        {
            //PendaftaranViewModel model = ResetPasswordRepo.ByEmail(Email); Ini untuk mengambil model saat mengubah password
            ViewBag.ResetFor = "Ganti Password";
            if (HttpContext.Session.GetString("Email") == model.Email)
            {
                argumentData.Email = model.Email;
                if (ModelState.IsValid)
                {
                    try
                    {
                        Random rand = new Random();
                        string digits = rand.Next(0, 999999).ToString("D6");
                        argumentData.Digits = digits;
                        string receiver = model.Email.ToString();
                        var senderEmail = new MailAddress("assazzainovic@gmail.com");
                        var receiverEmail = new MailAddress(receiver, "Receiver");
                        var password = "AnwalindaArsinta";
                        var sub = "OTP Code";
                        var body = digits;
                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(senderEmail.Address, password)
                        };
                        using (var mess = new MailMessage(senderEmail, receiverEmail)
                        {
                            Subject = sub,
                            Body = body
                        })
                        {
                            smtp.Send(mess);
                        }
                    }
                    catch (Exception)
                    {
                        ViewBag.Error = "Some Error";
                    }

                    return PartialView("_ChangePassOTP");
                }
            }
            return PartialView("_CheckEmail", model);
        }

        public IActionResult ChangePassOTP()
        {
            return PartialView("_ChangePassOTP");
        }

        [HttpPost]
        public IActionResult ChangePassOTP(string kodeOTP)
        {
            if (argumentData.Digits == kodeOTP)
            {
                return PartialView("_ChangePassword");
            }
            return PartialView("_ChangePassOTP");
        }

        public IActionResult ChangePassword()
        {
            return PartialView("_ChangePassword");
        }

        [HttpPost]
        public IActionResult ChangePassword(SetPasswordViewModel model)
        {
            if (model.Password == model.ConfirmPass)
            {
                ResponseResult result = ResetPasswordRepo.Edit(model, argumentData.Email);
                if (result.Success)
                {
                    ModelState.Clear();
                    return Ok(result);
                }
            }
            return PartialView("_ChangePassword");
        }
        #endregion

        public IActionResult Error()
        {
            return View();
        }

        public IActionResult Login()
        {
            return PartialView("_Login");
        }

        [HttpPost]
        public IActionResult Login(UserViewModel user)
        {
            if (ModelState.IsValid)
            {
                ResponseResult response = LoginRepo.Response(user);
                if (response.Success)
                {
                    ModelState.Clear();
                    var userGet = LoginRepo.GetUser(user.Email);
                    var biodataGet = LoginRepo.GetBiodata(userGet.Biodata_Id);
                    var roleGet = LoginRepo.GetRole(userGet.Role_Id);
                    var menuRoleGet = LoginRepo.GetMenuRole(roleGet.Id);
                    var menuGet = LoginRepo.GetMenu(menuRoleGet.Menu_Id);
                    ViewBag.Message = response.Message;
                    HttpContext.Session.SetString("Email", userGet.Email.ToString());
                    HttpContext.Session.SetString("BiodataId", userGet.Biodata_Id.ToString());
                    HttpContext.Session.SetString("Id", biodataGet.Id.ToString());
                    HttpContext.Session.SetString("Username", biodataGet.FullName);
                    HttpContext.Session.SetString("MenuId", menuGet.Id.ToString());
                    HttpContext.Session.SetString("Controller", menuGet.Name);
                    HttpContext.Session.SetString("userId", userGet.Id.ToString());

                    return PartialView("_Response", userGet);
                }
                ViewBag.Message = response.Message;
            }
            return PartialView("_Login", user);
        }

        [HttpPost]
        public IActionResult CheckRole()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            if (HttpContext.Session.GetString("Username") != null)
            {
                return RedirectToAction("Index", HttpContext.Session.GetString("Controller"));
            }
            return RedirectToAction("Index");
        }
    }
}
