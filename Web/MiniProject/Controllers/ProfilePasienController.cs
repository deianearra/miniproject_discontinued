﻿using DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using ViewModel;

namespace MiniProject.Controllers
{
    public class ProfilePasienController : Controller
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public ProfilePasienController(IHostingEnvironment environment)
        {
            hostingEnvironment = environment;
        }

        public IActionResult Index()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }



        public IActionResult Edit(int id)
        {
            BiodataViewModel model = ProfileRepo.ById(id);
            return PartialView("_Edit", model);
        }

        [HttpPost]
        public IActionResult Edit(BiodataViewModel model)
        {
            if (model.Image != null)
            {
                var uniqueFileName = GetUniqueFileName(model.Image.FileName);
                var uploads = Path.Combine(hostingEnvironment.WebRootPath, "Foto");
                var filePath = Path.Combine(uploads, uniqueFileName);
                model.Image.CopyTo(new FileStream(filePath, FileMode.Create));
                model.Image_Path = uniqueFileName;
                model.Modified_By = long.Parse(HttpContext.Session.GetString("Id"));
                bool success = ProfileRepo.Edit(model);
            }
            HttpContext.Session.SetString("Image_Path", model.Image_Path);

           /* /if (success) { }
            return RedirectToAction("Index");/*/
            
            return RedirectToAction("Profile");

        }

        private string GetUniqueFileName(string fileName)
        {
            fileName = Path.GetFileName(fileName);
            return Path.GetFileNameWithoutExtension(fileName)
                      + "_"
                      + Guid.NewGuid().ToString().Substring(0, 4)
                      + Path.GetExtension(fileName);

        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Profile()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.User = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Image = HttpContext.Session.GetString("Image_Path");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }

        public IActionResult TabPembayaran()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.User = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Image = HttpContext.Session.GetString("Image_Path");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }  

    }
}
