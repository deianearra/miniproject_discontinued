﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Collections.Generic;
using ViewModel;

namespace MiniProject.Controllers
{
    public class KatProdKesController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public IActionResult List()
        {
            return PartialView("_List", KatProdKesRepo.All());
        }
        public IActionResult ListPage(int page, int rows)
        {
            //int pPage = 1;
            //int pRows = 3;
            var prodKestList = KatProdKesRepo.All(page, rows);
            decimal totalPage = (decimal)prodKestList.Item2 / rows;
            ViewBag.Pages = Math.Ceiling(totalPage);
            return PartialView("_List", prodKestList.Item1);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }
        [HttpPost]
        public IActionResult Create(KategoriProdukKesehatanViewModel model)
        {
            bool result = KatProdKesRepo.Create(model);
            return Ok(result);
        }

        public IActionResult Edit(int id)
        {
            KategoriProdukKesehatanViewModel model = KatProdKesRepo.ById(id);
            return PartialView("_Edit", model);
        }
        [HttpPost]
        public IActionResult Edit(KategoriProdukKesehatanViewModel model)
        {
            bool result = KatProdKesRepo.Edit(model);
            return Ok(result);
        }

        public IActionResult Delete(int id)
        {
            KategoriProdukKesehatanViewModel item = KatProdKesRepo.ById(id);
            return PartialView("_Delete", item);
        }
        [HttpPost]
        public IActionResult Delete(KategoriProdukKesehatanViewModel model)
        {
            return Ok(KatProdKesRepo.Delete(model.Id));
        }
    }
}
