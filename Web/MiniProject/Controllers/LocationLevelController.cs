﻿using DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ViewModel;

namespace MiniProject.Controllers
{
    public class LocationLevelController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult List()
        {
            return PartialView("_List");
        }

        public IActionResult ListPage(int page, int rows, string search = "")
        {
            var locationList = LocationLevelRepo.All(page, rows, String.IsNullOrEmpty(search) ? "" : search);
            decimal totalPage = (decimal)locationList.Item2 / rows;
            ViewBag.Pages = Math.Ceiling(totalPage);
            return PartialView("_List", locationList.Item1);
        }

        public IActionResult Create()
        {

            return PartialView("_Create");
        }

        [HttpPost]
        public IActionResult Create(LocationLevelViewModel model)
        {
            model.Created_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = LocationLevelRepo.Create(model);
            return Ok(result);

        }

        public IActionResult Edit(long id)
        {
            LocationLevelViewModel model = LocationLevelRepo.ById(id);
            return PartialView("_Edit", model);
        }
        [HttpPost]
        public IActionResult Edit(LocationLevelViewModel model)
        {
            model.Modified_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = LocationLevelRepo.Edit(model);
            return Ok(result);
        }

        public IActionResult Delete(long id) //Delete disini bukan menghapus data, melainkan merubah kolom Is_deleted menjadi true
        {
            LocationLevelViewModel model = LocationLevelRepo.ById(id);
            List<LocationViewModel> list = LocationLevelRepo.CantDelete();
            foreach (LocationViewModel item in list)
            {
                if (item.Location_Level_Id == id)
                {
                    return PartialView("_NotDeleteable", model);
                }
            }

            return PartialView("_Delete", model);
        }

        [HttpPost]
        public IActionResult Delete(LocationLevelViewModel model)
        {
            model.Deleted_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = LocationLevelRepo.Delete(model);
            return Ok(result);
        }

    }
}
