﻿using DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using ViewModel;

namespace MiniProject.Controllers
{
    public class PasienController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            ViewBag.Username = HttpContext.Session.GetString("Username");
            if (HttpContext.Session.GetString("Username") == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                int id = int.Parse(HttpContext.Session.GetString("MenuId"));
                List<MenuViewModel> menuList = PasienRepo.All(id);
                return View(menuList);
            }
        }
        public IActionResult Cari_Dokter()
        {
            ViewBag.Location = PasienRepo.LocationCariDokter();
            ViewBag.Specialization = PasienRepo.SpecializationCariDokter();
            ViewBag.Treatment = PasienRepo.TreatmentCariDokter();
            return PartialView("_Cari_Dokter");
        }
        [HttpPost]
        public IActionResult Cari_Dokter(CariDokterViewModel model)
        {
            List<ListDokterViewModel> listDokter = PasienRepo.GetListDokter(model);
            TempData["ListDokter"] = listDokter;
            // 2. kenapa tidak langsung tampil ke halaman pencarian Cari_Dokter ?
            return PartialView("_Cari_Dokter");
        }
        public IActionResult List_Dokter()
        {
            var listDokter = TempData["ListDokter"];
            return View(listDokter);
        }
        public IActionResult Cari_Obat()
        {
             
            return PartialView("_Cari_Obat");
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
