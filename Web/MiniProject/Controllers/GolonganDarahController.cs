﻿using DataAccess;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ViewModel;

namespace MiniProject.Controllers
{
    public class GolonganDarahController : Controller
    {
        public IActionResult Index()
        {
            ViewBag.Id = HttpContext.Session.GetString("Id");
            ViewBag.UserName = HttpContext.Session.GetString("Username");
            ViewBag.Message = HttpContext.Session.GetString("Message");
            ViewBag.Controller = HttpContext.Session.GetString("Controller");
            if (ViewBag.UserName == null)
            {
                return RedirectToAction("Error", "Home");
            }
            else
            {
                return View();
            }
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }


        public IActionResult List()
        {
            //List<GolonganDarahViewModel> list = GolonganDarahRepo.All();
            return PartialView("_List"/*, list*/);
        }

        public IActionResult ListPage(int page, int rows, string search = "")
        {
            var golDarList = GolonganDarahRepo.All(page, rows, String.IsNullOrEmpty(search) ? "" : search);
            decimal totalPage = (decimal)golDarList.Item2 / rows;
            ViewBag.Pages = Math.Ceiling(totalPage);
            return PartialView("_List", golDarList.Item1);
        }
        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public IActionResult Create(GolonganDarahViewModel model)
        {
            //model.Created_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = GolonganDarahRepo.Create(model);
            return Ok(result);
        }

        public IActionResult Edit(long id)
        {
            GolonganDarahViewModel model = GolonganDarahRepo.ById(id);
            return PartialView("_Edit", model);
        }
        [HttpPost]
        public IActionResult Edit(GolonganDarahViewModel model)
        {
            model.Modified_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = GolonganDarahRepo.Edit(model);
            return Ok(result);
        }

        public IActionResult Delete(int id) //Delete disini bukan menghapus data, melainkan merubah kolom Is_deleted menjadi true
        {
            GolonganDarahViewModel model = GolonganDarahRepo.ById(id);
            //List<GolonganDarahViewModel> list = GolonganDarahRepo.CantDelete();
            //foreach (GolonganDarahViewModel item in list)
            //{
            //    if (item.location_level_id == id)
            //    {
            //        return PartialView("_NotDeleteable", model);
            //    }
            //}

            return PartialView("_Delete", model);
        }

        [HttpPost]
        public IActionResult Delete(GolonganDarahViewModel model)
        {
            model.Deleted_by = long.Parse(HttpContext.Session.GetString("userId"));
            bool result = GolonganDarahRepo.Delete(model);
            return Ok(result);
        }


    }
}
