﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using ViewModel;

namespace MiniProject.Controllers
{
    public class SegmenProdukKesehatanController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult List()
        {
            List<SegmenProdukKesehatanViewModel> list = SegmenProdukKesehatanRepo.All();
            return PartialView("_List", list);
        }

        public IActionResult Create()
        {
            return PartialView("_Create");
        }

        [HttpPost]
        public IActionResult Create(SegmenProdukKesehatanViewModel model)
        {
            bool result = SegmenProdukKesehatanRepo.Create(model);
            return Ok(result);
        }
    }
}
