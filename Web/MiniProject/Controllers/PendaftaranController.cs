﻿using DataAccess;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Mail;
using ViewModel;

namespace MiniProject.Controllers
{
    public class PendaftaranController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Daftar()
        {
            return PartialView("_Daftar");
        }


        public class argumentData
        {
            public static string Digits = string.Empty;
        }

        [HttpPost]
        public IActionResult Daftar(PendaftaranViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Random rand = new Random();
                    string digits = rand.Next(0, 999999).ToString("D6");
                    argumentData.Digits = digits;
                    string receiver = model.Email.ToString();
                    var senderEmail = new MailAddress("assazzainovic@gmail.com");
                    var receiverEmail = new MailAddress(receiver, "Receiver");
                    var password = "AnwalindaArsinta";
                    var sub = "OTP Code";
                    var body = digits;
                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(senderEmail.Address, password)
                    };
                    using (var mess = new MailMessage(senderEmail, receiverEmail)
                    {
                        Subject = sub,
                        Body = body
                    })
                    {
                        smtp.Send(mess);
                    }
                }
                catch (Exception)
                {
                    ViewBag.Error = "Some Error";
                }

                ResponseResult result = PendaftaranRepo.Create(model);
                if (result.Success)
                {
                    ModelState.Clear();
                    return PartialView("_VerifyOTP");
                }
            }
            return PartialView("_Daftar", model);
        }

        public IActionResult VerifyOTP()
        {
            return PartialView("_VerifyOTP");
        }

        [HttpPost]
        public IActionResult VerifyOTP(string kodeOTP)
        {
            if (argumentData.Digits == kodeOTP)
            {
                return PartialView("_SetPassWord");
            }
            return PartialView("_VerifyOTP");
        }

        public IActionResult SetPassword()
        {
            return PartialView("_SetPassWord");
        }

        [HttpPost]
        public IActionResult SetPassword(string password, string confirmPass)
        {
            if (password == confirmPass)
            {
                return PartialView("_BioDaftar");
            }
            return PartialView("_SetPassWord");
        }

        public IActionResult BioDaftar()
        {
            return PartialView("_BioDaftar");
        }
    }
}
