﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Katagori_Produk_Kesehatan
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }

        public long Deleted_by { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; } = false;

        //Foreign Key
        public virtual ICollection<Produk_Kesehatan> Produk_Kesehatans { get; set; }

    }
}
