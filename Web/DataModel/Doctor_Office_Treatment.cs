﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Doctor_Office_Treatment
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Doctor_Treatment_Id { get; set; }
        public long? Doctor_Office_Id { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Doctor_Treatment_Id")]
        public virtual Doctor_Treatment Doctor_Treatment { get; set; }
        [ForeignKey("Doctor_Office_Id")]
        public virtual Doctor_Office Doctor_Office { get; set; }
    }
}
