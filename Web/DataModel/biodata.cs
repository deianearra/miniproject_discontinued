﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class biodata
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(255)]
        public string FullName { get; set; }
        [MaxLength(15)]
        public string MobilePhone { get; set; }
        [MaxLength(255)]
        public string Image_Path { get; set; }

        // Base Properties
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public int? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        public virtual ICollection<user> Users { get; set; }
        public virtual ICollection<customer> Customer { get; set; }
        public virtual ICollection<biodata_address> Biodata_Addresses { get; set; }
    }
}
