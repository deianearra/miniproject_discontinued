﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModel.Migrations
{
    public partial class updateDoctor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Doctor_Educations_Doctors_Doctor_Id",
                table: "Doctor_Educations");

            migrationBuilder.DropForeignKey(
                name: "FK_Doctor_Educations_Education_Levels_Education_LevelId",
                table: "Doctor_Educations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Education_Levels",
                table: "Education_Levels");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Doctor_Educations",
                table: "Doctor_Educations");

            migrationBuilder.DropColumn(
                name: "Is_Delete",
                table: "m_specialization");

            migrationBuilder.RenameTable(
                name: "Education_Levels",
                newName: "Education_Level");

            migrationBuilder.RenameTable(
                name: "Doctor_Educations",
                newName: "Doctor_Education");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Educations_Education_LevelId",
                table: "Doctor_Education",
                newName: "IX_Doctor_Education_Education_LevelId");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Educations_Doctor_Id",
                table: "Doctor_Education",
                newName: "IX_Doctor_Education_Doctor_Id");

            migrationBuilder.AddColumn<bool>(
                name: "Is_Deleted",
                table: "m_specialization",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Education_Level",
                table: "Education_Level",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Doctor_Education",
                table: "Doctor_Education",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "location",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    Parent_Id = table.Column<long>(nullable: true),
                    Location_Level_Id = table.Column<long>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_location", x => x.Id);
                    table.ForeignKey(
                        name: "FK_location_m_location_level_Location_Level_Id",
                        column: x => x.Location_Level_Id,
                        principalTable: "m_location_level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_location_location_Parent_Id",
                        column: x => x.Parent_Id,
                        principalTable: "location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Medical_Facility_Category",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Medical_Facility_Category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "t_current_Doctor_Specializations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Id = table.Column<long>(nullable: true),
                    Specialization_Id = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_current_Doctor_Specializations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_current_Doctor_Specializations_Doctors_Doctor_Id",
                        column: x => x.Doctor_Id,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_t_current_Doctor_Specializations_m_specialization_Specialization_Id",
                        column: x => x.Specialization_Id,
                        principalTable: "m_specialization",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Medical_Facility_Category_Id = table.Column<long>(nullable: true),
                    Location_Id = table.Column<long>(nullable: true),
                    Full_Address = table.Column<string>(nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: true),
                    Phone_Code = table.Column<string>(maxLength: 10, nullable: true),
                    Phone = table.Column<string>(maxLength: 15, nullable: true),
                    Fax = table.Column<string>(maxLength: 15, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_medical_facility_location_Location_Id",
                        column: x => x.Location_Id,
                        principalTable: "location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_m_medical_facility_Medical_Facility_Category_Medical_Facility_Category_Id",
                        column: x => x.Medical_Facility_Category_Id,
                        principalTable: "Medical_Facility_Category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility_schedule",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Medical_Facility_Id = table.Column<long>(nullable: true),
                    Day = table.Column<string>(maxLength: 10, nullable: true),
                    Time_Schedule_Start = table.Column<string>(maxLength: 10, nullable: true),
                    Time_Schedule_End = table.Column<string>(maxLength: 10, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility_schedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_medical_facility_schedule_m_medical_facility_Medical_Facility_Id",
                        column: x => x.Medical_Facility_Id,
                        principalTable: "m_medical_facility",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Id = table.Column<long>(nullable: true),
                    Medical_Facility_Id = table.Column<long>(nullable: true),
                    Specialization = table.Column<string>(maxLength: 100, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_Doctors_Doctor_Id",
                        column: x => x.Doctor_Id,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_m_medical_facility_Medical_Facility_Id",
                        column: x => x.Medical_Facility_Id,
                        principalTable: "m_medical_facility",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office_schedule",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Id = table.Column<long>(nullable: true),
                    Medical_Facility_Schedule_Id = table.Column<long>(nullable: true),
                    Slot = table.Column<int>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office_schedule", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_schedule_Doctors_Doctor_Id",
                        column: x => x.Doctor_Id,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_schedule_m_medical_facility_schedule_Medical_Facility_Schedule_Id",
                        column: x => x.Medical_Facility_Schedule_Id,
                        principalTable: "m_medical_facility_schedule",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "t_doctor_office_treatment",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Treatment_Id = table.Column<long>(nullable: true),
                    Doctor_Office_Id = table.Column<long>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_doctor_office_treatment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_treatment_t_doctor_office_Doctor_Office_Id",
                        column: x => x.Doctor_Office_Id,
                        principalTable: "t_doctor_office",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_t_doctor_office_treatment_t_doctor_treatment_Doctor_Treatment_Id",
                        column: x => x.Doctor_Treatment_Id,
                        principalTable: "t_doctor_treatment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_location_Location_Level_Id",
                table: "location",
                column: "Location_Level_Id");

            migrationBuilder.CreateIndex(
                name: "IX_location_Parent_Id",
                table: "location",
                column: "Parent_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_medical_facility_Location_Id",
                table: "m_medical_facility",
                column: "Location_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_medical_facility_Medical_Facility_Category_Id",
                table: "m_medical_facility",
                column: "Medical_Facility_Category_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_medical_facility_schedule_Medical_Facility_Id",
                table: "m_medical_facility_schedule",
                column: "Medical_Facility_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_current_Doctor_Specializations_Doctor_Id",
                table: "t_current_Doctor_Specializations",
                column: "Doctor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_current_Doctor_Specializations_Specialization_Id",
                table: "t_current_Doctor_Specializations",
                column: "Specialization_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_Doctor_Id",
                table: "t_doctor_office",
                column: "Doctor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_Medical_Facility_Id",
                table: "t_doctor_office",
                column: "Medical_Facility_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_schedule_Doctor_Id",
                table: "t_doctor_office_schedule",
                column: "Doctor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_schedule_Medical_Facility_Schedule_Id",
                table: "t_doctor_office_schedule",
                column: "Medical_Facility_Schedule_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_treatment_Doctor_Office_Id",
                table: "t_doctor_office_treatment",
                column: "Doctor_Office_Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_doctor_office_treatment_Doctor_Treatment_Id",
                table: "t_doctor_office_treatment",
                column: "Doctor_Treatment_Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Doctor_Education_Doctors_Doctor_Id",
                table: "Doctor_Education",
                column: "Doctor_Id",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Doctor_Education_Education_Level_Education_LevelId",
                table: "Doctor_Education",
                column: "Education_LevelId",
                principalTable: "Education_Level",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Doctor_Education_Doctors_Doctor_Id",
                table: "Doctor_Education");

            migrationBuilder.DropForeignKey(
                name: "FK_Doctor_Education_Education_Level_Education_LevelId",
                table: "Doctor_Education");

            migrationBuilder.DropTable(
                name: "t_current_Doctor_Specializations");

            migrationBuilder.DropTable(
                name: "t_doctor_office_schedule");

            migrationBuilder.DropTable(
                name: "t_doctor_office_treatment");

            migrationBuilder.DropTable(
                name: "m_medical_facility_schedule");

            migrationBuilder.DropTable(
                name: "t_doctor_office");

            migrationBuilder.DropTable(
                name: "m_medical_facility");

            migrationBuilder.DropTable(
                name: "location");

            migrationBuilder.DropTable(
                name: "Medical_Facility_Category");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Education_Level",
                table: "Education_Level");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Doctor_Education",
                table: "Doctor_Education");

            migrationBuilder.DropColumn(
                name: "Is_Deleted",
                table: "m_specialization");

            migrationBuilder.RenameTable(
                name: "Education_Level",
                newName: "Education_Levels");

            migrationBuilder.RenameTable(
                name: "Doctor_Education",
                newName: "Doctor_Educations");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Education_Education_LevelId",
                table: "Doctor_Educations",
                newName: "IX_Doctor_Educations_Education_LevelId");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Education_Doctor_Id",
                table: "Doctor_Educations",
                newName: "IX_Doctor_Educations_Doctor_Id");

            migrationBuilder.AddColumn<bool>(
                name: "Is_Delete",
                table: "m_specialization",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Education_Levels",
                table: "Education_Levels",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Doctor_Educations",
                table: "Doctor_Educations",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Doctor_Educations_Doctors_Doctor_Id",
                table: "Doctor_Educations",
                column: "Doctor_Id",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Doctor_Educations_Education_Levels_Education_LevelId",
                table: "Doctor_Educations",
                column: "Education_LevelId",
                principalTable: "Education_Levels",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
