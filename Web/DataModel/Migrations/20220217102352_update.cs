﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModel.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Doctor_Treatments_Doctors_Doctor_Id",
                table: "Doctor_Treatments");

            migrationBuilder.DropForeignKey(
                name: "FK_menu_Roles_menus_Menu_Id",
                table: "menu_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_menus_menus_Parent_Id",
                table: "menus");

            migrationBuilder.DropIndex(
                name: "IX_users_Email",
                table: "users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Specializations",
                table: "Specializations");

            migrationBuilder.DropPrimaryKey(
                name: "PK_menus",
                table: "menus");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Doctor_Treatments",
                table: "Doctor_Treatments");

            migrationBuilder.RenameTable(
                name: "Specializations",
                newName: "m_specialization");

            migrationBuilder.RenameTable(
                name: "menus",
                newName: "m_menu");

            migrationBuilder.RenameTable(
                name: "Doctor_Treatments",
                newName: "t_doctor_treatment");

            migrationBuilder.RenameIndex(
                name: "IX_Specializations_Name",
                table: "m_specialization",
                newName: "IX_m_specialization_Name");

            migrationBuilder.RenameIndex(
                name: "IX_menus_Parent_Id",
                table: "m_menu",
                newName: "IX_m_menu_Parent_Id");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Treatments_Name",
                table: "t_doctor_treatment",
                newName: "IX_t_doctor_treatment_Name");

            migrationBuilder.RenameIndex(
                name: "IX_Doctor_Treatments_Doctor_Id",
                table: "t_doctor_treatment",
                newName: "IX_t_doctor_treatment_Doctor_Id");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "users",
                maxLength: 255,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(255)",
                oldMaxLength: 255);

            migrationBuilder.AlterColumn<int>(
                name: "Modified_By",
                table: "users",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Login_Attempt",
                table: "users",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "Is_Locked",
                table: "users",
                nullable: false,
                oldClrType: typeof(bool),
                oldType: "bit",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "users",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.AlterColumn<int>(
                name: "Deleted_By",
                table: "users",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Created_By",
                table: "users",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AlterColumn<int>(
                name: "Modified_By",
                table: "biodatas",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Deleted_By",
                table: "biodatas",
                nullable: true,
                oldClrType: typeof(long),
                oldType: "bigint",
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Created_By",
                table: "biodatas",
                nullable: false,
                oldClrType: typeof(long),
                oldType: "bigint");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_specialization",
                table: "m_specialization",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_m_menu",
                table: "m_menu",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_doctor_treatment",
                table: "t_doctor_treatment",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_users_Email",
                table: "users",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_m_menu_m_menu_Parent_Id",
                table: "m_menu",
                column: "Parent_Id",
                principalTable: "m_menu",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_menu_Roles_m_menu_Menu_Id",
                table: "menu_Roles",
                column: "Menu_Id",
                principalTable: "m_menu",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_t_doctor_treatment_Doctors_Doctor_Id",
                table: "t_doctor_treatment",
                column: "Doctor_Id",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_m_menu_m_menu_Parent_Id",
                table: "m_menu");

            migrationBuilder.DropForeignKey(
                name: "FK_menu_Roles_m_menu_Menu_Id",
                table: "menu_Roles");

            migrationBuilder.DropForeignKey(
                name: "FK_t_doctor_treatment_Doctors_Doctor_Id",
                table: "t_doctor_treatment");

            migrationBuilder.DropIndex(
                name: "IX_users_Email",
                table: "users");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_doctor_treatment",
                table: "t_doctor_treatment");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_specialization",
                table: "m_specialization");

            migrationBuilder.DropPrimaryKey(
                name: "PK_m_menu",
                table: "m_menu");

            migrationBuilder.RenameTable(
                name: "t_doctor_treatment",
                newName: "Doctor_Treatments");

            migrationBuilder.RenameTable(
                name: "m_specialization",
                newName: "Specializations");

            migrationBuilder.RenameTable(
                name: "m_menu",
                newName: "menus");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_treatment_Name",
                table: "Doctor_Treatments",
                newName: "IX_Doctor_Treatments_Name");

            migrationBuilder.RenameIndex(
                name: "IX_t_doctor_treatment_Doctor_Id",
                table: "Doctor_Treatments",
                newName: "IX_Doctor_Treatments_Doctor_Id");

            migrationBuilder.RenameIndex(
                name: "IX_m_specialization_Name",
                table: "Specializations",
                newName: "IX_Specializations_Name");

            migrationBuilder.RenameIndex(
                name: "IX_m_menu_Parent_Id",
                table: "menus",
                newName: "IX_menus_Parent_Id");

            migrationBuilder.AlterColumn<string>(
                name: "Password",
                table: "users",
                type: "nvarchar(255)",
                maxLength: 255,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 255,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "Modified_By",
                table: "users",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Login_Attempt",
                table: "users",
                type: "int",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<bool>(
                name: "Is_Locked",
                table: "users",
                type: "bit",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "Deleted_By",
                table: "users",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "Created_By",
                table: "users",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<long>(
                name: "Modified_By",
                table: "biodatas",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "Deleted_By",
                table: "biodatas",
                type: "bigint",
                nullable: true,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<long>(
                name: "Created_By",
                table: "biodatas",
                type: "bigint",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_Doctor_Treatments",
                table: "Doctor_Treatments",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Specializations",
                table: "Specializations",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_menus",
                table: "menus",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_users_Email",
                table: "users",
                column: "Email",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Doctor_Treatments_Doctors_Doctor_Id",
                table: "Doctor_Treatments",
                column: "Doctor_Id",
                principalTable: "Doctors",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_menu_Roles_menus_Menu_Id",
                table: "menu_Roles",
                column: "Menu_Id",
                principalTable: "menus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_menus_menus_Parent_Id",
                table: "menus",
                column: "Parent_Id",
                principalTable: "menus",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
