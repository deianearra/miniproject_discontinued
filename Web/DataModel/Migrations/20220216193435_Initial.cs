﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DataModel.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "biodatas",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(maxLength: 255, nullable: true),
                    MobilePhone = table.Column<string>(maxLength: 15, nullable: true),
                    Image_Path = table.Column<string>(maxLength: 255, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_biodatas", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Education_Levels",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 10, nullable: false),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Education_Levels", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_blood_group",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Code = table.Column<string>(nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Created_By = table.Column<long>(maxLength: 50, nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(maxLength: 50, nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_blood_group", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_customer_relation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer_relation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_location_level",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Abbreviation = table.Column<string>(maxLength: 50, nullable: true),
                    Created_by = table.Column<long>(nullable: false),
                    Created_on = table.Column<DateTime>(nullable: false),
                    Modified_by = table.Column<long>(nullable: true),
                    Modified_on = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: true),
                    Deleted_on = table.Column<DateTime>(nullable: true),
                    Is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_location_level", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_facility_category",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_facility_category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item_category",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item_category", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item_segmentation",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Created_By = table.Column<long>(maxLength: 50, nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(maxLength: 50, nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: false),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item_segmentation", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "menus",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 20, nullable: true),
                    Url = table.Column<string>(maxLength: 50, nullable: true),
                    Parent_Id = table.Column<long>(nullable: true),
                    Big_Icon = table.Column<string>(maxLength: 100, nullable: true),
                    Small_Icon = table.Column<string>(maxLength: 100, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_menus", x => x.Id);
                    table.ForeignKey(
                        name: "FK_menus_menus_Parent_Id",
                        column: x => x.Parent_Id,
                        principalTable: "menus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "roles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 20, nullable: false),
                    Code = table.Column<string>(maxLength: 20, nullable: false),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Specializations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Specializations", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Wallet_Default_Nominals",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nominal = table.Column<int>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Wallet_Default_Nominals", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "biodata_Addresses",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Biodata_Id = table.Column<long>(nullable: true),
                    Label = table.Column<string>(maxLength: 100, nullable: true),
                    Recipient = table.Column<string>(maxLength: 100, nullable: true),
                    Recipient_Phone_Number = table.Column<string>(maxLength: 15, nullable: true),
                    Location_Id = table.Column<int>(nullable: true),
                    Postal_Code = table.Column<string>(maxLength: 10, nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_biodata_Addresses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_biodata_Addresses_biodatas_Biodata_Id",
                        column: x => x.Biodata_Id,
                        principalTable: "biodatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "customers",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Biodata_Id = table.Column<long>(nullable: true),
                    Dob = table.Column<DateTime>(nullable: true),
                    Gender = table.Column<int>(maxLength: 1, nullable: true),
                    Blood_Group_Id = table.Column<long>(nullable: true),
                    Rhesus_Type = table.Column<string>(maxLength: 5, nullable: true),
                    Height = table.Column<decimal>(nullable: true),
                    Weight = table.Column<decimal>(nullable: true),
                    Created_By = table.Column<int>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<int>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<int>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_customers_biodatas_Biodata_Id",
                        column: x => x.Biodata_Id,
                        principalTable: "biodatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Doctors",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Biodata_Id = table.Column<long>(nullable: true),
                    Str = table.Column<string>(maxLength: 50, nullable: false),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctors", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctors_biodatas_Biodata_Id",
                        column: x => x.Biodata_Id,
                        principalTable: "biodatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "m_customer",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Biodata_Id = table.Column<long>(nullable: false),
                    Dob = table.Column<DateTime>(nullable: false),
                    Gender = table.Column<string>(maxLength: 1, nullable: true),
                    Blood_group_id = table.Column<long>(nullable: false),
                    Rhesus_type = table.Column<string>(maxLength: 5, nullable: true),
                    Height = table.Column<decimal>(nullable: false),
                    Weight = table.Column<decimal>(nullable: false),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_customer_biodatas_Biodata_Id",
                        column: x => x.Biodata_Id,
                        principalTable: "biodatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_m_customer_m_blood_group_Blood_group_id",
                        column: x => x.Blood_group_id,
                        principalTable: "m_blood_group",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "m_location",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    parent_id = table.Column<long>(nullable: true),
                    location_level_id = table.Column<long>(nullable: true),
                    Created_by = table.Column<long>(nullable: false),
                    Created_on = table.Column<DateTime>(nullable: false),
                    Modified_by = table.Column<long>(nullable: true),
                    Modified_on = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: true),
                    Deleted_on = table.Column<DateTime>(nullable: true),
                    Is_deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_location", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_location_m_location_level_location_level_id",
                        column: x => x.location_level_id,
                        principalTable: "m_location_level",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_m_location_m_location_parent_id",
                        column: x => x.parent_id,
                        principalTable: "m_location",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "m_medical_item",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Medical_item_category_Id = table.Column<long>(nullable: false),
                    Composition = table.Column<string>(nullable: true),
                    Medical_item_segmentation_Id = table.Column<long>(nullable: false),
                    Manufacturer = table.Column<string>(maxLength: 100, nullable: true),
                    Indication = table.Column<string>(nullable: true),
                    Dosage = table.Column<string>(nullable: true),
                    Directions = table.Column<string>(nullable: true),
                    Contraindication = table.Column<string>(nullable: true),
                    Caution = table.Column<string>(nullable: true),
                    Packaging = table.Column<string>(maxLength: 50, nullable: true),
                    Price_max = table.Column<long>(nullable: false),
                    Price_min = table.Column<long>(nullable: false),
                    Image = table.Column<string>(nullable: true),
                    Image_path = table.Column<string>(maxLength: 100, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_medical_item", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_medical_item_m_medical_item_category_Medical_item_category_Id",
                        column: x => x.Medical_item_category_Id,
                        principalTable: "m_medical_item_category",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "menu_Roles",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Menu_Id = table.Column<long>(nullable: true),
                    Role_Id = table.Column<long>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_menu_Roles", x => x.Id);
                    table.ForeignKey(
                        name: "FK_menu_Roles_menus_Menu_Id",
                        column: x => x.Menu_Id,
                        principalTable: "menus",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_menu_Roles_roles_Role_Id",
                        column: x => x.Role_Id,
                        principalTable: "roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "users",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Biodata_Id = table.Column<long>(nullable: true),
                    Role_Id = table.Column<long>(nullable: true),
                    Email = table.Column<string>(maxLength: 100, nullable: false),
                    Password = table.Column<string>(maxLength: 255, nullable: false),
                    Login_Attempt = table.Column<int>(nullable: true),
                    Is_Locked = table.Column<bool>(nullable: true),
                    Last_Login = table.Column<DateTime>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_users_biodatas_Biodata_Id",
                        column: x => x.Biodata_Id,
                        principalTable: "biodatas",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_users_roles_Role_Id",
                        column: x => x.Role_Id,
                        principalTable: "roles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customer_Wallets",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Id = table.Column<long>(nullable: true),
                    Pin = table.Column<string>(maxLength: 6, nullable: true),
                    Balance = table.Column<decimal>(nullable: true),
                    Barcode = table.Column<string>(maxLength: 50, nullable: true),
                    Points = table.Column<decimal>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer_Wallets", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_Wallets_customers_Customer_Id",
                        column: x => x.Customer_Id,
                        principalTable: "customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "m_customer_member",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Id = table.Column<long>(nullable: false),
                    Customer_relation_id = table.Column<long>(nullable: false),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: false),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_by = table.Column<long>(nullable: false),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_m_customer_member", x => x.Id);
                    table.ForeignKey(
                        name: "FK_m_customer_member_customers_Customer_Id",
                        column: x => x.Customer_Id,
                        principalTable: "customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_m_customer_member_m_customer_relation_Customer_relation_id",
                        column: x => x.Customer_relation_id,
                        principalTable: "m_customer_relation",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Doctor_Educations",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Id = table.Column<long>(nullable: true),
                    Education_Level_Id = table.Column<long>(nullable: true),
                    Institution_Name = table.Column<string>(maxLength: 100, nullable: false),
                    Major = table.Column<string>(maxLength: 100, nullable: false),
                    Start_year = table.Column<string>(maxLength: 4, nullable: false),
                    End_year = table.Column<string>(maxLength: 4, nullable: false),
                    Is_Last_Education = table.Column<bool>(nullable: true),
                    Education_LevelId = table.Column<long>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor_Educations", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctor_Educations_Doctors_Doctor_Id",
                        column: x => x.Doctor_Id,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Doctor_Educations_Education_Levels_Education_LevelId",
                        column: x => x.Education_LevelId,
                        principalTable: "Education_Levels",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Doctor_Treatments",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Doctor_Id = table.Column<long>(nullable: true),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Delete = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Doctor_Treatments", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Doctor_Treatments_Doctors_Doctor_Id",
                        column: x => x.Doctor_Id,
                        principalTable: "Doctors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Customer_Wallet_Top_Ups",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Customer_Wallet_Id = table.Column<long>(nullable: true),
                    Amount = table.Column<decimal>(nullable: true),
                    Created_By = table.Column<long>(nullable: false),
                    Created_On = table.Column<DateTime>(nullable: false),
                    Modified_By = table.Column<long>(nullable: true),
                    Modified_On = table.Column<DateTime>(nullable: true),
                    Deleted_By = table.Column<long>(nullable: true),
                    Deleted_On = table.Column<DateTime>(nullable: true),
                    Is_Deleted = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customer_Wallet_Top_Ups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customer_Wallet_Top_Ups_Customer_Wallets_Customer_Wallet_Id",
                        column: x => x.Customer_Wallet_Id,
                        principalTable: "Customer_Wallets",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_biodata_Addresses_Biodata_Id",
                table: "biodata_Addresses",
                column: "Biodata_Id");

            migrationBuilder.CreateIndex(
                name: "IX_biodata_Addresses_Recipient_Phone_Number",
                table: "biodata_Addresses",
                column: "Recipient_Phone_Number",
                unique: true,
                filter: "[Recipient_Phone_Number] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_biodatas_MobilePhone",
                table: "biodatas",
                column: "MobilePhone",
                unique: true,
                filter: "[MobilePhone] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_Wallet_Top_Ups_Customer_Wallet_Id",
                table: "Customer_Wallet_Top_Ups",
                column: "Customer_Wallet_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Customer_Wallets_Customer_Id",
                table: "Customer_Wallets",
                column: "Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_customers_Biodata_Id",
                table: "customers",
                column: "Biodata_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_Educations_Doctor_Id",
                table: "Doctor_Educations",
                column: "Doctor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_Educations_Education_LevelId",
                table: "Doctor_Educations",
                column: "Education_LevelId");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_Treatments_Doctor_Id",
                table: "Doctor_Treatments",
                column: "Doctor_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Doctor_Treatments_Name",
                table: "Doctor_Treatments",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Doctors_Biodata_Id",
                table: "Doctors",
                column: "Biodata_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_blood_group_Code",
                table: "m_blood_group",
                column: "Code",
                unique: true,
                filter: "[Code] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_m_customer_Biodata_Id",
                table: "m_customer",
                column: "Biodata_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_customer_Blood_group_id",
                table: "m_customer",
                column: "Blood_group_id");

            migrationBuilder.CreateIndex(
                name: "IX_m_customer_member_Customer_Id",
                table: "m_customer_member",
                column: "Customer_Id");

            migrationBuilder.CreateIndex(
                name: "IX_m_customer_member_Customer_relation_id",
                table: "m_customer_member",
                column: "Customer_relation_id");

            migrationBuilder.CreateIndex(
                name: "IX_m_location_location_level_id",
                table: "m_location",
                column: "location_level_id");

            migrationBuilder.CreateIndex(
                name: "IX_m_location_parent_id",
                table: "m_location",
                column: "parent_id");

            migrationBuilder.CreateIndex(
                name: "IX_m_location_level_Name",
                table: "m_location_level",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_m_medical_item_Medical_item_category_Id",
                table: "m_medical_item",
                column: "Medical_item_category_Id");

            migrationBuilder.CreateIndex(
                name: "IX_menu_Roles_Menu_Id",
                table: "menu_Roles",
                column: "Menu_Id");

            migrationBuilder.CreateIndex(
                name: "IX_menu_Roles_Role_Id",
                table: "menu_Roles",
                column: "Role_Id");

            migrationBuilder.CreateIndex(
                name: "IX_menus_Parent_Id",
                table: "menus",
                column: "Parent_Id");

            migrationBuilder.CreateIndex(
                name: "IX_roles_Code",
                table: "roles",
                column: "Code",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_roles_Name",
                table: "roles",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Specializations_Name",
                table: "Specializations",
                column: "Name",
                unique: true,
                filter: "[Name] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_users_Biodata_Id",
                table: "users",
                column: "Biodata_Id");

            migrationBuilder.CreateIndex(
                name: "IX_users_Email",
                table: "users",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_users_Role_Id",
                table: "users",
                column: "Role_Id");

            migrationBuilder.CreateIndex(
                name: "IX_Wallet_Default_Nominals_Nominal",
                table: "Wallet_Default_Nominals",
                column: "Nominal",
                unique: true,
                filter: "[Nominal] IS NOT NULL");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "biodata_Addresses");

            migrationBuilder.DropTable(
                name: "Customer_Wallet_Top_Ups");

            migrationBuilder.DropTable(
                name: "Doctor_Educations");

            migrationBuilder.DropTable(
                name: "Doctor_Treatments");

            migrationBuilder.DropTable(
                name: "m_customer");

            migrationBuilder.DropTable(
                name: "m_customer_member");

            migrationBuilder.DropTable(
                name: "m_location");

            migrationBuilder.DropTable(
                name: "m_medical_facility_category");

            migrationBuilder.DropTable(
                name: "m_medical_item");

            migrationBuilder.DropTable(
                name: "m_medical_item_segmentation");

            migrationBuilder.DropTable(
                name: "menu_Roles");

            migrationBuilder.DropTable(
                name: "Specializations");

            migrationBuilder.DropTable(
                name: "users");

            migrationBuilder.DropTable(
                name: "Wallet_Default_Nominals");

            migrationBuilder.DropTable(
                name: "Customer_Wallets");

            migrationBuilder.DropTable(
                name: "Education_Levels");

            migrationBuilder.DropTable(
                name: "Doctors");

            migrationBuilder.DropTable(
                name: "m_blood_group");

            migrationBuilder.DropTable(
                name: "m_customer_relation");

            migrationBuilder.DropTable(
                name: "m_location_level");

            migrationBuilder.DropTable(
                name: "m_medical_item_category");

            migrationBuilder.DropTable(
                name: "menus");

            migrationBuilder.DropTable(
                name: "roles");

            migrationBuilder.DropTable(
                name: "customers");

            migrationBuilder.DropTable(
                name: "biodatas");
        }
    }
}
