﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Doctor
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        
        [Required, MaxLength(50)]
        public string Str { get; set; }

        // Base Properties. ? merupakan required untuk tipe data numerik
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; } = false;

        [ForeignKey("Biodata_Id")]
        public virtual biodata Biodata { get; set; }

        public virtual ICollection<Doctor_Education> Doctor_Educations { get; set; }
        public virtual ICollection<Doctor_Treatment> Treaments { get; set; }
    }
}
