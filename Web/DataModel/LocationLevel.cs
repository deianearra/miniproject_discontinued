﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class LocationLevel
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(50)]
        public string? Name { get; set; }
        [MaxLength(50)]
        public string? Abbreviation { get; set; }
        [Required]
        public long Created_by { get; set; }
        [Required]
        public DateTime Created_on { get; set; }
        public long? Modified_by { get; set; }
        public DateTime? Modified_on { get; set; }
        public long? Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool Is_deleted { get; set; } = false;

        // foreign key
        public virtual ICollection<Lokasi> Location { get; set; }

    }
}
