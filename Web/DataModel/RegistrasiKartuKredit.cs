﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataModel
{
    public class RegistrasiKartuKredit
    {
        public long Id { get; set; }

        public long Customer_Id { get; set; }
        [Required, MaxLength(16)]
        public string Card_Number { get; set; }
        [Required]
        public DateTime Validity_Period { get; set; }
        [Required]
        public string CVV { get; set; }

        [Required, MaxLength(50)]
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }

        [MaxLength(50)]
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long Deleted_by { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
    }
}
