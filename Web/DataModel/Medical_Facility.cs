﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Medical_Facility
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(50)]
        public string? Name { get; set; }
        public long? Medical_Facility_Category_Id { get; set; }
        public long? Location_Id { get; set; }
        public string? Full_Address { get; set; }
        [MaxLength(100)]
        public string? Email { get; set; }
        [MaxLength(10)]
        public string? Phone_Code { get; set; }
        [MaxLength(15)]
        public string? Phone { get; set; }
        [MaxLength(15)]
        public string? Fax { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Medical_Facility_Category_Id")]
        public virtual Medical_Facility_Category Medical_Facility_Category { get; set; }
        [ForeignKey("Location_Id")]
        public virtual location location { get; set; }

        public virtual ICollection<Doctor_Office> Doctor_Office { get; set; }
        public virtual ICollection<Medical_Facility_Schedule> Medical_Facility_Schedule { get; set; }
    }
}
