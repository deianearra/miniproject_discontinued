﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class menu_role
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Menu_Id { get; set; }
        public long? Role_Id { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // foreignkey
        [ForeignKey("Menu_Id")]
        public virtual menu Menu { get; set; }
        [ForeignKey("Role_Id")]
        public virtual role Role { get; set; }
    }
}
