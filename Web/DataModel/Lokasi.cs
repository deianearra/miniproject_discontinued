﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Lokasi
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(100)]
        public string? Name { get; set; }
        public long? parent_id { get; set; }
        public long? location_level_id { get; set; }
        [Required]
        public long Created_by { get; set; }
        [Required]
        public DateTime Created_on { get; set; }
        public long? Modified_by { get; set; }
        public DateTime? Modified_on { get; set; }
        public long? Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool Is_deleted { get; set; }

        [ForeignKey("parent_id")]
        public virtual Lokasi Location { get; set; }

        [ForeignKey("location_level_id")]
        public virtual LocationLevel LocationLevel { get; set; }
    }
}
