﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Customer_Wallet
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Customer_Id { get; set; }
        [MaxLength(6)]
        public string Pin { get; set; }
        public decimal? Balance { get; set; }
        [MaxLength(50)]
        public string Barcode { get; set; }
        public decimal? Points { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; }

        [ForeignKey("Customer_Id")]
        public virtual customer Customer { get; set; }

        public virtual ICollection<Customer_Wallet_Top_Up> Customer_Wallet_Top_Ups { get; set; }
    }
}
