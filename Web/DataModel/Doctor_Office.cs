﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Doctor_Office
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Doctor_Id { get; set; }
        public long? Medical_Facility_Id { get; set; }
        [MaxLength(100)]
        public string? Specialization { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Doctor_Id")]
        public virtual Doctor Doctor { get; set; }
        [ForeignKey("Medical_Facility_Id")]
        public virtual Medical_Facility Medical_Facility { get; set; }

        public virtual ICollection<Doctor_Office_Treatment> Doctor_Office_Treatment { get; set; }
    }
}
