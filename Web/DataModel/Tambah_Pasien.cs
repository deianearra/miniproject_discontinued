﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Tambah_Pasien
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long Customer_Id { get; set; }
        public long Customer_relation_id { get; set; }
        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }

        public long Deleted_by { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
        //Foreign Key
        [ForeignKey("Customer_Id")]
        public virtual customer Customer { get; set; }
        [ForeignKey("Customer_relation_id")]
        public virtual Hubungan_Pasien Hubungan_Pasien { get; set; }


    }
}
