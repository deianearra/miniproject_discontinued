﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Pasien
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long Biodata_Id { get; set; }
        public DateTime Dob { get; set; }
        [MaxLength(1)]
        public string Gender { get; set; }
        public long Blood_group_id { get; set; }
        [MaxLength(5)]
        public string Rhesus_type { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }

        public long Deleted_by { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; }

        //Foreign Key

        [ForeignKey("Biodata_Id")]
        public virtual biodata Biodata { get; set; }
        [ForeignKey("Blood_group_id")]
        public virtual Golongan_Darah Golongan_Darah { get; set; }

    }
}
