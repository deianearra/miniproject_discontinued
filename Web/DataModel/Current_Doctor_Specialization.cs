﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Current_Doctor_Specialization
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Doctor_Id { get; set; }
        public long? Specialization_Id { get; set; }

        [ForeignKey("Doctor_Id")]
        public virtual Doctor Doctor { get; set; }

        [ForeignKey("Specialization_Id")]
        public virtual Specialization Specialization{ get; set; }
    }
}
