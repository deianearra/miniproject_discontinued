﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class location
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        [MaxLength(100)]
        public string? Name { get; set; }
        public long? Parent_Id { get; set; }
        public long? Location_Level_Id { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Parent_Id")]
        public virtual location Location { get; set; }
        [ForeignKey("Location_Level_Id")]
        public virtual LocationLevel LocationLevel { get; set; }
        public virtual ICollection<Medical_Facility> Medical_Facilities { get; set; }
    }
}