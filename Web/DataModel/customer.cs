﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class customer
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        public DateTime? Dob { get; set; }
        [MaxLength(1)]
        public int? Gender { get; set; }
        public long? Blood_Group_Id { get; set; }
        [MaxLength(5)]
        public string? Rhesus_Type { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }

        // Base Properties
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public int Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Biodata_Id")]
        public virtual biodata Biodata { get; set; }
        /*[ForeignKey("Blood_Group_Id")]
        public virtual blood_group_id Blood_Group_Id { get; set; }*/
    }
}
