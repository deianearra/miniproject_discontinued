﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class user
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        public long? Role_Id { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(255)]
        public string Password { get; set; }
        public int Login_Attempt { get; set; }
        public bool Is_Locked { get; set; }
        public DateTime? Last_Login { get; set; }

        // Base Properties
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public int? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key

        [ForeignKey("Biodata_Id")]
        public virtual biodata Biodata { get; set; }
        [ForeignKey("Role_Id")]
        public virtual role Role { get; set; }
    }
}
