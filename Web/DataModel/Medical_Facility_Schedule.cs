﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Medical_Facility_Schedule
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Medical_Facility_Id { get; set; }
        [MaxLength(10)]
        public string? Day { get; set; }
        [MaxLength(10)]
        public string? Time_Schedule_Start { get; set; }
        [MaxLength(10)]
        public string? Time_Schedule_End { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // Foreign Key
        [ForeignKey("Medical_Facility_Id")]
        public virtual Medical_Facility Medical_Facility { get; set; }
        public virtual ICollection<Doctor_Office_Schedule> Doctor_Office_Schedule { get; set; }
    }
}
