﻿using Microsoft.EntityFrameworkCore;
using System;

namespace DataModel
{
    public class XsisPosDbContext: DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //base.OnConfiguring(optionsBuilder);
            optionsBuilder.UseSqlServer(@"Data Source=LAPTOP-PLU61B1K;Initial Catalog=DB_MINI_PROJECT;Integrated Security=True");
        }
        public DbSet<Katagori_Produk_Kesehatan> m_medical_item_category { get; set; }
        public DbSet<biodata> biodatas { get; set; }
        public DbSet<biodata_address> biodata_Addresses { get; set; }
        public DbSet<customer> customers { get; set; }
        public DbSet<menu> m_menu { get; set; }
        public DbSet<menu_role> menu_Roles { get; set; }
        public DbSet<role> roles { get; set; }
        public DbSet<user> users { get; set; }
        public DbSet<LocationLevel> m_location_level { get; set; }
        public DbSet<Lokasi> m_location { get; set; }
        public DbSet<Golongan_Darah> m_blood_group { get; set; }
        public DbSet<Produk_Kesehatan> m_medical_item { get; set; }
        public DbSet<Kategori_Fasilitas_Kesehatan> m_medical_facility_category { get; set; }
        public DbSet<Pasien> m_customer { get; set; }
        public DbSet<Tambah_Pasien> m_customer_member { get; set; }
        public DbSet<Hubungan_Pasien> m_customer_relation { get; set; }
        public DbSet<Segmen_Produk_Kesehatan> m_medical_item_segmentation { get; set; }
        public DbSet<Specialization> m_specialization { get; set; }
        public DbSet<Customer_Wallet> Customer_Wallets { get; set; }
        public DbSet<Customer_Wallet_Top_Up> Customer_Wallet_Top_Ups { get; set; }
        public DbSet<Doctor> Doctors { get; set; }
        public DbSet<Doctor_Treatment> t_doctor_treatment { get; set; }
        public DbSet<Wallet_Default_Nominal> Wallet_Default_Nominals { get; set; }

        public DbSet<Education_Level> Education_Levels { get; set; }
        public DbSet<Doctor_Education> Doctor_Educations { get; set; }

        public DbSet<Medical_Facility> m_medical_facility { get; set; }

        public DbSet<Medical_Facility_Schedule> m_medical_facility_schedule { get; set; }
        public DbSet<Doctor_Education> m_doctor_education { get; set; }
        public DbSet<Doctor_Office> t_doctor_office { get; set; }
        public DbSet<Doctor_Office_Schedule> t_doctor_office_schedule { get; set; }
        public DbSet<Doctor_Office_Treatment> t_doctor_office_treatment { get; set; }
        public DbSet<Current_Doctor_Specialization> t_current_Doctor_Specializations { get; set; }
        public DbSet<Education_Level> m_education_level { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<biodata>()
                .HasIndex(b => b.MobilePhone)
                .IsUnique();
            modelBuilder.Entity<biodata_address>()
                .HasIndex(a => a.Recipient_Phone_Number)
                .IsUnique();
            modelBuilder.Entity<role>()
                .HasIndex(r => r.Name)
                .IsUnique();
            modelBuilder.Entity<user>()
                .HasIndex(u => u.Email)
                .IsUnique();
            modelBuilder.Entity<Golongan_Darah>()
               .HasIndex(o => o.Code)
               .IsUnique();
            modelBuilder.Entity<Specialization>()
                .HasIndex(b => b.Name)
                .IsUnique();
            modelBuilder.Entity<Doctor_Treatment>()
                .HasIndex(b => b.Name)
                .IsUnique();
            modelBuilder.Entity<Wallet_Default_Nominal>()
                .HasIndex(b => b.Nominal)
                .IsUnique();
            modelBuilder.Entity<biodata_address>()
                .HasIndex(a => a.Recipient_Phone_Number)
                .IsUnique();
            modelBuilder.Entity<role>()
                .HasIndex(r => r.Name)
                .IsUnique();
            modelBuilder.Entity<role>()
                .HasIndex(r => r.Code)
                .IsUnique();
            modelBuilder.Entity<user>()
                .HasIndex(u => u.Email)
                .IsUnique();
            modelBuilder.Entity<LocationLevel>()
               .HasIndex(o => o.Name)
               .IsUnique();
        }
    }
}
