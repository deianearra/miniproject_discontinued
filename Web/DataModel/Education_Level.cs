﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Education_Level
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [Required, MaxLength(10)]
        public string Name { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; } = false;

        public virtual ICollection<Doctor_Education> Doctor_Educations { get; set; }
    }
}
