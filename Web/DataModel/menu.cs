﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class menu
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(20)]
        public string? Name { get; set; }
        [MaxLength(50)]
        public string? Url { get; set; }
        public long? Parent_Id { get; set; }
        [MaxLength(100)]
        public string? Big_Icon { get; set; }
        [MaxLength(100)]
        public string? Small_Icon { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        // foreign key
        [ForeignKey("Parent_Id")]
        public virtual menu Menu { get; set; }
        public virtual ICollection<menu_role> Menu_Role { get; set; }
    }
}
