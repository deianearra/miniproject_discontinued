﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataModel
{
    public class Produk_Kesehatan
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public long Medical_item_category_Id { get; set; }
        public string Composition { get; set; }
        public long Medical_item_segmentation_Id { get; set; }
        [MaxLength(100)]
        public string Manufacturer { get; set; }
        public string Indication { get; set; }
        public string Dosage { get; set; }
        public string Directions { get; set; }
        public string Contraindication { get; set; }
        public string Caution { get; set; }
        [MaxLength(50)]
        public string Packaging { get; set; }
        public long Price_max { get; set; }
        public long Price_min { get; set; }
        public string Image { get; set; }
        [MaxLength(100)]
        public string Image_path { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }

        public long Deleted_by { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Delete { get; set; } = false;

        [ForeignKey("Medical_item_category_Id")]
        public virtual Katagori_Produk_Kesehatan Katagori_Produk_Kesehatan { get; set; }
    }
}
