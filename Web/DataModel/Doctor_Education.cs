﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Doctor_Education
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Doctor_Id { get; set; }
        public long? Education_Level_Id { get; set; }
        [Required, MaxLength(100)]
        public string Institution_Name { get; set; }
        [Required, MaxLength(100)]
        public string Major { get; set; }
        [Required, MaxLength(4)]
        public string Start_year { get; set; }
        [Required, MaxLength(4)]
        public string End_year { get; set; }
        public bool? Is_Last_Education { get; set; } = false;

        //Foreign Key
        [ForeignKey("Doctor_Id")]
        public virtual Doctor Doctor { get; set; }
        public virtual Education_Level Education_Level { get; set; }
    }
}
