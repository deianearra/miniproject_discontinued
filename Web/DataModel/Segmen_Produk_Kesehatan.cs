﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class Segmen_Produk_Kesehatan
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }

        public string Name { get; set; }

        [Required, MaxLength(50)]
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }

        [MaxLength(50)]
        public long Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long Deleted_by { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
    }
}
