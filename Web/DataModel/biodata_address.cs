﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataModel
{
    public class biodata_address
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        [MaxLength(100)]
        public string? Label { get; set; }
        [MaxLength (100)]
        public string? Recipient { get; set; }
        [MaxLength(15)]
        public string? Recipient_Phone_Number { get; set; }
        public int? Location_Id { get; set; }
        [MaxLength(10)]
        public string? Postal_Code { get; set; }
        public string? Address { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; }

        // Foreign Key
        [ForeignKey("Biodata_Id")]
        public virtual biodata Biodata { get; set; }
        //[ForeignKey("Location_Id")]
        //public virtual location Location { get; set; }
    }
}
