﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class ListDokterViewModel
    {
        // dari m_biodata
        public string FullName { get; set; }
        // dari m_specialization
        public string Specialization_Name { get; set; }
        public int Doctor_Experience { get; set; }
        public string Med_Fac_Cat_Name { get; set; }
        // dari m_medical_facility
        public string Med_Fac_Name { get; set; }
        public string Location_Name { get; set; }
        // dari m_biodata
        public string Image_Path { get; set; }
        public string Treatment_Name { get; set; }
    }
}