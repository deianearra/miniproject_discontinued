﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class DokterProfilViewModel
    {
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        [MaxLength(50)]
        public string Str { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
    }
}
