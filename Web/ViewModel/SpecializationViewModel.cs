﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class SpecializationViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Nama Spesialisasi")]
        [Required] //Di buat untuk create. Jika di submit kosong maka akan gagal dalam database
        public string Name { get; set; } 

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }

    }
}
