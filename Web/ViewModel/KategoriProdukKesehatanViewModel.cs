﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ViewModel
{
    public class KategoriProdukKesehatanViewModel
    {
        public long Id { get; set; }

        [Display(Name = " Nama Produk")]
        public string Name { get; set; }

        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
        public long Deleted_by { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
    }
}
