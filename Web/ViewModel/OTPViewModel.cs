﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class OTPViewModel
    {
        [Required, MaxLength(255)]
        public string OTP { get; set; }
    }
}
