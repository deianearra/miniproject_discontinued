﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class MenuViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string Btn_Id { get; set; }
        [Display(Name="Parent ID")]
        public long Parent_Id { get; set; }
        [Display(Name = "Big Icon")]
        public string Big_Icon { get; set; }
        [Display(Name = "Small Icon")]
        public string Small_Icon { get; set; }
        public bool Is_Deleted { get; set; }
    }
}
