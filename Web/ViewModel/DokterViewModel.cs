﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class DokterViewModel
    {
        public long Id { get; set; }
        public long? Biodata_Id { get; set; }
        [MaxLength(50)]
        public string Str { get; set; }
    }
}
