﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class RegistrasiKartuKreditViewModel
    {
        public long Id { get; set; }

        public long Customer_Id { get; set; }
        [Display(Name = "Card Number")]
        public string Card_Number { get; set; }
        [Display(Name = "Validity Period")]
        public DateTime Validity_Period { get; set; }
  
        public string CVV { get; set; }
    }
}
