﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class RoleViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
    }
}
