﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class UserViewModel
    {
        [Required]
        public long Id { get; set; }
        [Required,Display(Name ="Biodata ID")]
        public long Biodata_Id { get; set; }
        [Required,Display(Name  = "Role ID")]
        public long Role_Id { get; set; }
        [Required,EmailAddress]
        public string Email { get; set; }
        [Required,StringLength(255),DataType(DataType.Password)]
        public string Password { get; set; }
        [Display(Name  = "Login Attempt")]
        public int Login_Attempt { get; set; }
        [Display(Name  = "Is Locked ?")]
        public bool Is_Locked { get; set; }
        [Display(Name  = "Last Login")]
        public DateTime Last_Login { get; set; }
        [Display(Name = "Is Deleted ?")]
        public bool Is_Deleted { get; set; }
    }
}
