﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class DoctorEducationViewModel
    {
        public long Id { get; set; }
        public long? Doctor_Id { get; set; }
        public long? Education_Level_Id { get; set; }
        [MaxLength(100)]
        public string Institution_Name { get; set; }
        [MaxLength(100)]
        public string Major { get; set; }
        [ MaxLength(4)]
        public string Start_year { get; set; }
        [MaxLength(4)]
        public string End_year { get; set; }
        public bool? Is_Last_Education { get; set; } = false;
    }
}
