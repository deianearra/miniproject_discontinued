﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class BiodataAddressViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Biodata ID")]
        public long? Biodata_Id { get; set; }
        public string Label { get; set; }
        public string Recipient { get; set; }
        [Display(Name = "Recipient Phone Number")]
        public string Recipient_Phone_Number { get; set; }
        [Display(Name = "Location ID")]
        public long? Location_Id { get; set; }
        [Display(Name = "Postal Code")]
        public string Postal_Code { get; set; }
        public string Address { get; set; }
    }
}
