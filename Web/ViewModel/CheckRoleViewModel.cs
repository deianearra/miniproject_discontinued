﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class CheckRoleViewModel
    {
        public string Message { get; set; }
        public long Id { get; set; }
        public string Username { get; set; }
        public string Controller { get; set; }
    }
}
