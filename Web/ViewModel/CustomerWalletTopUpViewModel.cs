﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class CustomerWalletTopUpViewModel
    {
        public long Id { get; set; }

        public long? Customer_Wallet_Id { get; set; }
        public decimal? Amount { get; set; }

        // Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; }

    }
}
