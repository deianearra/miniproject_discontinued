﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class NominalDompetElektronikViewModel
    {
        public long Id { get; set; }
        //public long Customer_wallet_id { get; set; }
        [Display(Name = "Nominal")]
        public int? Nominal { get; set; }
        
        //Base Properties
        public long Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public long? Modified_By { get; set; }
        public DateTime Modified_On { get; set; }
        public long? Deleted_By { get; set; }
        public DateTime Deleted_On { get; set; }
        public bool Is_Delete { get; set; }
    }
}
