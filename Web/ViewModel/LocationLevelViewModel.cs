﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ViewModel
{
    public class LocationLevelViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Nama*")]
        public string Name { get; set; }

        [Display(Name = "Nama Singkat")]
        public string Abbreviation { get; set; }
        public long Created_by { get; set; }
        public DateTime Created_on { get; set; }
        public long? Modified_by { get; set; }
        public DateTime? Modified_on { get; set; }
        public long? Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool Is_deleted { get; set; }
    }
}

