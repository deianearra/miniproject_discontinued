﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class CustomerViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Biodata ID")]
        public long Biodata_Id { get; set; }
        [Display(Name = "Date of Birthday")]
        public DateTime Dob { get; set; }
        public string Gender { get; set; }
        [Display(Name = "")]
        public int Blood_Group_Id { get; set; }
        [Display(Name = "Rhesus Type")]
        public string Rhesus_Type { get; set; }
        public decimal Height { get; set; }
        public decimal Weight { get; set; }
    }
}
