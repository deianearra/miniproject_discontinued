﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class GolonganDarahViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Tipe Golongan Darah")]
        public string Code { get; set; }

        public string Description { get; set; }
        public long Created_by { get; set; }
        public DateTime Created_on { get; set; }
        public long? Modified_by { get; set; }
        public DateTime? Modified_on { get; set; }
        public long? Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool Is_delete { get; set; }
    }
}
