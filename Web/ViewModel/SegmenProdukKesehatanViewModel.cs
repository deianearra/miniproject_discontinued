﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class SegmenProdukKesehatanViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Segmen Produk Kesehatan")]
        public string Name { get; set; }

    }
}
