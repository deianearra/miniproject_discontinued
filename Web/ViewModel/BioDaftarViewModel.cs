﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class BioDaftarViewModel
    {
        public long Id { get; set; }

        [MaxLength(255), Display(Name = "Full Name")]
        public string FullName { get; set; }

        [MaxLength(255), Display(Name = "Mobile Phone")]
        public string MobilePhone { get; set; }

        public long? RoleId { get; set; }

        [MaxLength(255)]
        public string RoleName { get; set; }

        // Base Properties
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public int? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;

        //public List<UserRoleViewModel> UserRoles { get; set; }
    }

    //public class UserRoleViewModel
    //{
    //    [MaxLength(255)]
    //    public long? RoleId { get; set; }

    //    [MaxLength(255)]
    //    public string RoleName { get; set; }
    //}
}
