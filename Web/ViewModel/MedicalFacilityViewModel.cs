﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ViewModel
{
    public class MedicalFacilityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Medical_Facility_Category_Id { get; set; }
        public int Location_Id { get; set; }
        public string Full_Address { get; set; }
        public string Email { get; set; }
        public string Phone_Code { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
    }
}
