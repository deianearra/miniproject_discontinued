﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class LocationViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        [Display(Name = "Parent Id")]
        public long Parent_Id { get; set; }
        [Display(Name = "Location Level Id")]
        public long Location_Level_Id { get; set; }


    }
}
