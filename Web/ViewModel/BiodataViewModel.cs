﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class BiodataViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Mobile Phone")]
        public string MobilePhone { get; set; }

        [Display(Name = "Pilih Gambar")]
        public IFormFile Image { get; set; }

        [Display(Name = "Image sebelum")]
        public string Image_Path { get; set; }

        public long Created_by { get; set; }
        public DateTime Created_on { get; set; }
        public long? Modified_By { get; set; }
        public DateTime? Modified_on { get; set; }
        public long? Deleted_by { get; set; }
        public DateTime? Deleted_on { get; set; }
        public bool Is_deleted { get; set; }
    }
}
