﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class CariObatViewModel
    {
        
        public List<LocationViewModel> Location { get; set; }
        public int Location_Id { get; set; }
        [Display(Name = "Doctor Name")]
        public string Doctor_Name { get; set; }
        public List<SpecializationViewModel> Specialization { get; set; }
        public int Specialization_Id { get; set; }
        public List<DoctorTreatmentViewModel> Treatment { get; set; }
        public int Treatment_Id { get; set; }
    }
}
