﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class MenuRoleViewModel
    {
        public long Id { get; set; }
        [Display(Name = "Menu ID")]
        public long Menu_Id { get; set; }
        [Display(Name = "Role ID")]
        public long Role_Id { get; set; }
    }
}
