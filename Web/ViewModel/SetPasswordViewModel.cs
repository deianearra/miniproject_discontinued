﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ViewModel
{
    public class SetPasswordViewModel
    {
        public long Id { get; set; }

        [Required, MaxLength(255)]
        public string Password { get; set; }
        
        [Required, MaxLength(255)]
        public string ConfirmPass { get; set; }

        // Base Properties
        public int Created_By { get; set; }
        public DateTime Created_On { get; set; }
        public int? Modified_By { get; set; }
        public DateTime? Modified_On { get; set; }
        public int? Deleted_By { get; set; }
        public DateTime? Deleted_On { get; set; }
        public bool Is_Deleted { get; set; } = false;
    }
}
